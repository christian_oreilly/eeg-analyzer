# -*- coding: utf-8 -*-
"""
Created on Fri Oct 18 17:49:12 2013

@author: oreichri
"""



from server import ProcessingServer


def main():
    server = ProcessingServer()    
    print "Server started on " + server.getAddress()    
    server.listen()



if __name__ == "__main__":
    main()