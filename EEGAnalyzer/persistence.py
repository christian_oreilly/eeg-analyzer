# -*- coding: utf-8 -*-
"""
Created on Tue Nov 05 17:46:11 2013

@author: oreichri
"""



import cPickle as pickle
import os
import collections


class PersistenceRegister:
    
    def __init__(self):
        self._lists     = {}
        self._objects   = {}
    
    def getList(self, listName):
        if not listName in self._lists:
            self._lists[listName] = []
        return self._lists[listName]
        
    def addToList(self, listName, value):
        
        if listName in self._lists:   
            if value in self._lists[listName]:
                self._lists[listName].remove(value)
            self._lists[listName].insert(0, value)                       
        else:
            self._lists[listName] = [value]
        
        pickle.dump(self, open(pickleFile, "wb" ) )
    
    
    def getObject(self, objectName):
        if not objectName in self._objects:
            self._objects[objectName] = None
        return self._objects[objectName]
        
    def setObject(self, objectName, value):
        self._objects[objectName] = value                     
        pickle.dump(self, open(pickleFile, "wb" ) )


    def __repr__(self):
        return str(self.__dict__)
        
        
        





class PersistentProperty(object):
    def __init__(self, name, func, initval=None):
        self.func      = func
        self.attr_name = '__' + name
        setattr(self, self.attr_name, initval)
        
    def __get__(self, instance, owner):
        return getattr(self, self.attr_name)
        
    def __set__(self, instance, value):
        setattr(self, self.attr_name, value)
        self.func(instance)
        



class SchemaPersistObject:
    
    def __init__(self, filePath, schemaDictName, schemaDictObj):
        self.isRunning  = False
        
        self._schemaDictName    = schemaDictName
        self._schemaDictObj     = schemaDictObj
        self.filePath           = filePath
        self.registerName      = "register"
        self.registerPath       = os.path.join(os.path.dirname(filePath), "register.bin")

    def persistenceManagement(self):
        persistReg.setObject(self._schemaDictName , self._schemaDictObj)   

    filePath               = PersistentProperty('filePath', persistenceManagement, "")
    requestedNbProcesses   = PersistentProperty('requestedNbProcesses', persistenceManagement, 1)
    registerName           = PersistentProperty('registerName', persistenceManagement, "register" ) 
    registerPath           = PersistentProperty('registerPath', persistenceManagement, "")  



class SchemaDict(collections.MutableMapping):  
    
    def __init__(self, name, *args, **kwargs):
        self.store = dict()
        self.update(dict(*args, **kwargs))  # use the free update to set keys
        
        self.__name                 = name 


    def __getitem__(self, key):
        return self.store[self.__keytransform__(key)]

    def __setitem__(self, key, value):
        
        self.store[self.__keytransform__(key)] = value
        persistReg.setObject(self.__name, self)

    def __delitem__(self, key):
        del self.store[self.__keytransform__(key)]
        persistReg.setObject(self.__name, self)
        
    def __iter__(self):
        return iter(self.store)

    def __len__(self):
        return len(self.store)

    def __keytransform__(self, key):
        return key

                
                
                
pickleFile = "persist.reg"

try:
    persistReg = pickle.load(open(pickleFile))
  
except IOError:
    """
    The pickling file probably does not exist (e.g. it is the first time the
    program is launched). Start with a new blank copy.
    """
    persistReg = PersistenceRegister()

except AttributeError:
    """
    Probablly that the structure of the program has changed. Start with a new 
    blank copy.
    """        
    persistReg = PersistenceRegister()
    
                