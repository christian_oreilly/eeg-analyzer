# -*- coding: utf-8 -*-
"""
Created on Thu Oct 17 23:27:37 2013

@author: Christian
"""


from PySide import QtCore, QtGui
import socket

from PyBlockWork import ProcessingNode
from EEGAnalyzer import ProcessingClient

class ProcessingNodeWidget(QtGui.QWidget):
    
    def __init__(self, parent = None):
        super(ProcessingNodeWidget, self).__init__(parent)     
        self.mainWindow = parent         
      
        self.remoteNodes = []     
        self.node        = None  
      
        self.nbProcessEdit     = QtGui.QLineEdit("1")     
        self.nbProcessEdit.setValidator(QtGui.QIntValidator())

        self.startLocalBtn     = QtGui.QPushButton("Start")
        self.stopLocalBtn      = QtGui.QPushButton("Stop")
        
        self.startLocalBtn.clicked.connect(self.startLocalNode)
        self.stopLocalBtn.clicked.connect(self.stopLocalNode)

        self.stopLocalBtn.setEnabled(False)
        
        self.setLayout(QtGui.QGridLayout())
      
        self.layout().addWidget(QtGui.QLabel("Local node:"), 0, 0, 1, 1)
        self.layout().addWidget(self.nbProcessEdit, 0, 1, 1, 1)
        self.layout().addWidget(self.startLocalBtn, 0, 2, 1, 1)
        self.layout().addWidget(self.stopLocalBtn, 0, 3, 1, 1)

        
        self.addRemoteBtn      = QtGui.QPushButton("Add remote")
        self.addRemoteBtn.clicked.connect(self.addRemoteNode)

        self.consoleTabWidget     = QtGui.QTabWidget()    

        self.layout().addWidget(self.addRemoteBtn,     1, 3, 1, 1)
        self.layout().addWidget(self.consoleTabWidget, 1, 0, 1, 3)


    def addRemoteNode(self): 
        self.remoteNodes.append(RemoteProcessingNodeWidget(self.mainWindow))
        self.consoleTabWidget.addTab(self.remoteNodes[-1], str(len(self.remoteNodes)))     
        

    def startLocalNode(self):    
        
        if self.mainWindow.checkNameServer():
            
            HMAC_KEY = str(self.mainWindow.pyroNSWidget.hmacKeyWidget.text())
            self.node = ProcessingNode(int(self.nbProcessEdit.text()), 
                                          "stagingRoom", "communicationHub", HMAC_KEY)
            self.startLocalBtn.setEnabled(False)
            self.stopLocalBtn.setEnabled(True)



    def stopLocalNode(self):
        if not self.node is None:
            self.node.terminate(force=True)
            self.startLocalBtn.setEnabled(True)
            self.stopLocalBtn.setEnabled(False)











class RemoteProcessingNodeWidget(QtGui.QWidget):
    
    def __init__(self, mainWindow, parent = None, IP=None):
        super(RemoteProcessingNodeWidget, self).__init__(parent)     
        self.mainWindow = mainWindow         
      
        self.nbProcessEdit     = QtGui.QLineEdit("1")    
        self.nbProcessEdit.setValidator(QtGui.QIntValidator())
        
        self.IPEdit     = QtGui.QLineEdit("")    
        self.IPEdit.setInputMask("000.000.000.000;_")
        

        self.connectBtn     = QtGui.QPushButton("Connect")
        self.disconnectBtn  = QtGui.QPushButton("Disconnect")
        self.startBtn       = QtGui.QPushButton("Start")
        self.startBtn       = QtGui.QPushButton("Start")
        self.stopBtn        = QtGui.QPushButton("Stop")
        
        self.startBtn.clicked.connect(self.startNode)
        self.stopBtn.clicked.connect(self.stopNode)
        
        self.connectBtn.clicked.connect(self.connectToServer)
        self.disconnectBtn.clicked.connect(self.disconnectFromServer)

        self.stopBtn.setEnabled(False)
        self.startBtn.setEnabled(False)
        self.disconnectBtn.setEnabled(False)
        
        self.setLayout(QtGui.QHBoxLayout())
      
        self.layout().addWidget(QtGui.QLabel("IP:"))
        self.layout().addWidget(self.IPEdit)
        self.layout().addWidget(QtGui.QLabel("Nb. processes:"))
        self.layout().addWidget(self.nbProcessEdit)
        self.layout().addWidget(self.connectBtn)
        self.layout().addWidget(self.disconnectBtn)
        self.layout().addWidget(self.startBtn)
        self.layout().addWidget(self.stopBtn)


    def connectToServer(self):
        self.processingClient = ProcessingClient(str(self.IPEdit.text()))
        
        try:
            self.processingClient.connect()
        except socket.error, e:
             QtGui.QMessageBox.critical(self, "Connection refused", 
                                 "The connection unsuccessful for the requested address. Error: " + str(e))            

        self.stopBtn.setEnabled(False)
        self.startBtn.setEnabled(True)
        self.disconnectBtn.setEnabled(True)
        self.connectBtn.setEnabled(False)    
    
    
    def disconnectFromServer(self):
        self.processingClient.close()

        self.stopBtn.setEnabled(False)
        self.startBtn.setEnabled(False)
        self.disconnectBtn.setEnabled(False)
        self.connectBtn.setEnabled(True)    
            


    def startNode(self, stagingRoomName = "stagingRoom", comHubName="communicationHub"):    
        
        self.mainWindow.checkNameServer()        
        
        try:
            self.processingClient.sendCommand("set HMAC_KEY " + str(self.mainWindow.pyroNSWidget.hmacKeyWidget.text()))
            self.processingClient.sendCommand("checkNameServer")
            self.processingClient.sendCommand("set stagingRoomName " + stagingRoomName)
            self.processingClient.sendCommand("set comHubName " + comHubName)
            self.processingClient.sendCommand("set nbProcessingNodes " + self.nbProcessEdit.text())
            self.processingClient.sendCommand("startProcessingNodes")                                             
                                                     
            self.startBtn.setEnabled(False)
            self.stopBtn.setEnabled(True)
        except ValueError, e:
             QtGui.QMessageBox.critical(self, "Error starting the processing note", str(e))          


    def stopNode(self):
        self.processingClient.sendCommand("stopProcessingNodes")     
        self.startBtn.setEnabled(True)
        self.stopBtn.setEnabled(False)


