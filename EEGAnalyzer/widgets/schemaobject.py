# -*- coding: utf-8 -*-
"""
Created on Thu Jul 11 14:29:27 2013

@author: Christian
"""


from PySide.QtCore import *
from PySide.QtGui import *



"""
   Abstract class derived to implement every object that are to be displayed
   on the SchemaCaneva. It has a name and a code representation
   (although both can be empty).
"""
class SchemaObject(QGraphicsObject): #(QFrame): #QGraphicsRectItem

    # signals
    selectedSchemaObjectChanged = Signal(object)   
     


    def __init__(self, parent=None) :
        super(SchemaObject, self).__init__(parent)
        #self.parent = parent

    
    
        # For managing the script code associated with the object.
        # Private, not protected because we want to control the
        # assignation of its value to connect the updateObjectCode
        # signal on assignation.
        self.objectCode = None


    def mapTo(self, holder, point):
        return super(SchemaObject, self).mapTo(holder, QPoint(point.x, point.y))

    def getName(self)       : return ""

    #def parentWidget(self)  : return self.parent


    def getObjectCode(self) : return m_pObjectCode
    
    # For walking though the schema.
    #virtual unsigned int    getNbPreviousObject()           = 0;
    #virtual unsigned int    getNbNextObject()               = 0;
    #virtual SchemaObjectPtr previousObject(int r_iIndex)    = 0;
    #virtual SchemaObjectPtr nextObject(int r_iIndex)        = 0;


    
    #def setParent(self, parent) :
        #self.parent = parent
        #QFrame.setParent(self, parent)
    
    def focusInEvent(self, event) :
        # To update the property panel
        self.selectedSchemaObjectChanged.emit(self)


    def emitSelectedSchemaObjectChanged(self, objectSelected):
        self.selectedSchemaObjectChanged.emit(objectSelected)
 
    
    def setObjectCode(self, objectCode):
        self.objectCode = objectCode
    
        if self.objectCode :
            self.objectCode.updateObjectCode.connect(self.formatObjectCode())
    
    
    def getObjectCodeStr(self):
        if self.objectCode is None:
            return ""
    
        self.formatObjectCode()
        return self.objectCode.str()


"""



    @property
    def x(self):
        return super(SchemaObject, self).x() 

    @x.setter
    def x(self, x):
        self.setX(x)
        

    @property
    def y(self):
        return super(SchemaObject, self).y()         

    @y.setter
    def y(self, y):
        self.setY(y)
        
        
    
    

    @property
    def width(self):
        return 0#self.geometry().width()

    @property
    def height(self):
        return 0#self.geometry().height()
"""