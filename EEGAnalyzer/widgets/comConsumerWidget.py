# -*- coding: utf-8 -*-
"""
Created on Fri Oct 18 13:53:59 2013

@author: oreichri
"""



from PySide import QtGui, QtCore
import numpy as np
from widgets.consoleWidget import ConsoleWidget

from PyBlockWork.pyroWarehouse import warehouse

class ComConsumerWidget(QtGui.QWidget):
    
    def __init__(self, parent = None):
        super(ComConsumerWidget, self).__init__(parent)     
        self.mainWindow = parent         
      
        self.consoleTabWidget     = QtGui.QTabWidget()    

        self.consoles = {}
        
        self.setLayout(QtGui.QHBoxLayout())
      
        self.layout().addWidget(self.consoleTabWidget )



    def connectHub(self, HMAC_KEY, comHubName = "communicationHub"):
                
        self.HMAC_KEY = HMAC_KEY
        self.comHubName = comHubName
        self.messageConsumer = MessageConsumer(HMAC_KEY, comHubName, self)        
        self.messageConsumer.start()     


    @QtCore.Slot(str)        
    def removeConsole(self, ID):
        index = self.consoleTabWidget.indexOf(self.consoles[ID])
        self.consoleTabWidget.removeTab(index)
        del self.consoles[ID]


    @QtCore.Slot(str)        
    def addConsole(self, ID):
        self.consoles[ID] = ConsoleWidget()
        self.consoleTabWidget.addTab(self.consoles[ID], ID)

    @QtCore.Slot(str, str)        
    def addMessage(self, ID, message):
        self.consoles[ID].addMessage(message)



        



class MessageConsumer(QtCore.QThread) :   
    
    addConsole    = QtCore.Signal(str)
    removeConsole = QtCore.Signal(str)
    addMessage    = QtCore.Signal(str, str)    

    def __init__(self, HMAC_KEY, comHubName, consumerWidget):        
        super(MessageConsumer, self).__init__()     

        self.consumerWidget = consumerWidget

        self.addConsole.connect(self.consumerWidget.addConsole) 
        self.removeConsole.connect(self.consumerWidget.removeConsole) 
        self.addMessage.connect(self.consumerWidget.addMessage) 

        self.HMAC_KEY = HMAC_KEY
        self.comHubName = comHubName        
        
        self.consoleIDs = [] 
        
    
    def run(self):
        
        self.moveToThread(QtGui.QApplication.instance().thread())        

        while(True):
            
            readerIDs = warehouse.proxy(self.comHubName).getReaderIDs()
            for ID in readerIDs:

                # Adding new workers
                if not ID in self.consoleIDs:
                    self.addConsole.emit(ID)
                    self.consoleIDs.append(ID)

                # Read messages. The comHub object is not locked. Thus, 
                # the worker ID could have been removed between the time
                # we got a list of valir readerIDs and the time the 
                # messages are read. If this happens, a KeyError will be 
                # raised. We can ignore this message.
                try:
                    while not warehouse.proxy(self.comHubName).empty(ID) :
                        self.addMessage.emit(ID, warehouse.proxy(self.comHubName).readMessage(ID))                     
                except KeyError:
                    pass
                    
            # Remove retired workers.
            workerIsRetired = np.logical_not(np.in1d(self.consoleIDs, readerIDs))
            retiredWorkerIDs = np.array(self.consoleIDs)[workerIsRetired]
                       
                       
            for ID in retiredWorkerIDs:
                self.removeConsole.emit(ID)
                self.consoleIDs.remove(ID)
                
            QtCore.QThread.sleep(1)
                        

