# -*- coding: utf-8 -*-
"""
Created on Thu Jul 11 15:10:41 2013

@author: Christian
"""


from PySide.QtCore import *
from PySide.QtGui import *
from PySide import QtCore, QtGui

from schemaobject import SchemaObject
from sizepoint import SizePoint, SIZEPOINT_TOP, SIZEPOINT_BOTTOM, SIZEPOINT_LEFT, SIZEPOINT_RIGHT
from constant import PORT_SIZE
from port import Port, __PORT_TYPE_INPUT__,  __PORT_TYPE_OUTPUT__

from PyBlockWork import Block

class ProcessBlock(SchemaObject): #SchemaObject

    ###########################################################################
    # SIGNALS
    ###########################################################################
    
    
    selectedSchemaObjectChanged = Signal(object)
    blockFocusInEvent           = Signal(Block)
    blockFocusOutEvent          = Signal()
    
    updateEvent = Signal(QRegion)
    
    
    # Emitted when this schema object has been moved such that other objects
    # can update accordingly.
    schemaObjectMoved = Signal()


    def __init__(self, parent=None, name = "", block = None):

        super(ProcessBlock, self).__init__(parent)
       
        self.viewHasFocus   = False

        # FOR THE DRAG AND DROP OPERATION
        self.bDragging      = False
        self.lastMousePos   = None
        self.selected       = False

        # FOR THE APPEARANCE
        #self.lblBox = QLabel(self)
        self.lblBox  = QGraphicsTextItem(self)
        self.rect    = QRectF()
    
        # FOR RESIZING
        self.resizePointsTop            = SizePoint(self, SIZEPOINT_TOP)
        self.resizePointsBottom         = SizePoint(self, SIZEPOINT_BOTTOM)
        self.resizePointsLeft           = SizePoint(self, SIZEPOINT_LEFT)
        self.resizePointsRight          = SizePoint(self, SIZEPOINT_RIGHT)
        self.resizePointsTopRight       = SizePoint(self, SIZEPOINT_RIGHT | SIZEPOINT_TOP)
        self.resizePointsTopLeft        = SizePoint(self, SIZEPOINT_LEFT  | SIZEPOINT_TOP)
        self.resizePointsBottomRight    = SizePoint(self, SIZEPOINT_RIGHT | SIZEPOINT_BOTTOM)
        self.resizePointsBottomLeft     = SizePoint(self, SIZEPOINT_LEFT  | SIZEPOINT_BOTTOM)

        # Name of the block. It must be unique whithin a schema.
        if name == "" and not block is None:
            self.name = block.name
        else:
            self.name = name   
            
        self.block = block
    
        # List of input and output ports
        self.inputPorts  = {}
        self.outputPorts = {}      
            
        self.blockStatus = None
    
    
        self.setAcceptDrops(True)        
   
        #self.setFocusPolicy(Qt.StrongFocus)

        #palette = QPalette()
        #palette.setColor(QPalette.Foreground, Qt.red)
        #self.setPalette(palette);

        # Setting the block name
        self.lblBox.setPlainText(self.name)
        #self.lblBox.setText(self.name)
        #self.lblBox.setStyleSheet("border: 1px solid black")
        #self.lblBox.setStyleSheet("background: white")
        #self.lblBox.setFrameStyle(QFrame.Box)
        #self.lblBox.setAlignment(Qt.AlignVCenter | Qt.AlignHCenter)

        self.setFlag(QGraphicsItem.ItemIsSelectable, True)
        self.setFlag(QGraphicsItem.ItemIsMovable, True)
        self.setFlag(QGraphicsItem.ItemIsFocusable, True)

    @property
    def x(self):
        return super(SchemaObject, self).x() 

    @x.setter
    def x(self, x):
        self.prepareGeometryChange()
        self.setX(x)        
        self.geometryChanged()
        

    @property
    def y(self):
        return super(SchemaObject, self).y()         

    @y.setter
    def y(self, y):
        self.prepareGeometryChange()
        self.setY(y)
        self.geometryChanged()
     
        
    @property
    def width(self):
        return self.rect.width()

    @property
    def height(self):
        return self.rect.height()


    def viewGotFocus(self):
        self.viewHasFocus = True

    def viewLostFocus(self):
        self.viewHasFocus = False



    def guessBestGeometry(self, pos=None):
        

        metrics     = QFontMetrics(self.lblBox.font())
        pixelWidth  = max(metrics.boundingRect(self.lblBox.toPlainText()).width()*1.2,                20) + 4*PORT_SIZE
        pixelHeight = max(max(len(self.inputPorts), len(self.outputPorts))*(PORT_SIZE+10), 20) + 2*PORT_SIZE
        
        if pos is None:
            pos = self.rect
            
        self.rect = QRectF(pos.x(), pos.y(), pixelWidth, pixelHeight)

        self.prepareGeometryChange()        
        self.geometryChanged()

    

    def boundingRect(self):
        return self.rect


    def paint(self, painter, option, widget):
        pen = QPen()
        pen.setColor(QtCore.Qt.black)
        pen.setWidth(1)
        painter.setPen(pen)
        
        if self.blockStatus is None:
            painter.setBrush(QBrush(QColor(200, 200, 255, 128)))      
        elif self.blockStatus == "started":
            painter.setBrush(QBrush(QColor(255, 255, 0, 128)))      
        elif self.blockStatus == "inError":
            painter.setBrush(QBrush(QColor(128, 128, 128, 128)))      
        elif self.blockStatus == "executed":
            painter.setBrush(QBrush(QColor(255, 0, 0, 128)))      
            
        painter.drawRect(self.rect.adjusted(PORT_SIZE, PORT_SIZE, -PORT_SIZE, -PORT_SIZE))

        
        if self.hasFocus():
            pen.setColor(QtCore.Qt.red)
            pen.setWidth(1)
            painter.setPen(pen)
            painter.setBrush(QBrush(QColor(0, 0, 0, 0)))              
            painter.drawRect(self.rect)
            
            
            

    def setLastMousePos(self, pos) :
        self.lastMousePos = pos


    def addInputPort(self, name):
        self.inputPorts[name] = Port(name, self, __PORT_TYPE_INPUT__ )
        self.inputPorts[name].updateEvent.connect(self.updateEvent.emit)    
        self.geometryChanged()


    def addOutputPort(self, name):
        self.outputPorts[name] = Port(name, self, __PORT_TYPE_OUTPUT__)
        self.outputPorts[name].updateEvent.connect(self.updateEvent.emit)
        self.geometryChanged()

    
    
    
    def setResizeVisible(self, isVisible):
        self.resizePointsTop.setVisible(isVisible)
        self.resizePointsBottom.setVisible(isVisible)
        self.resizePointsLeft.setVisible(isVisible)
        self.resizePointsRight.setVisible(isVisible)
        self.resizePointsTopRight.setVisible(isVisible)
        self.resizePointsTopLeft.setVisible(isVisible)
        self.resizePointsBottomRight.setVisible(isVisible)
        self.resizePointsBottomLeft.setVisible(isVisible)
    
    def previousObject(self, iIndex):
        raise
        if iIndex >= self.getNbPreviousObject() :
            return None
    
        if not self.inputPorts[iIndex].isLinked() :
            return None
    
        return self.inputPorts[iIndex].getLinkedPort().parentWidget()
    
    
    def nextObject(self, iIndex):
        raise        
        if iIndex >= self.getNbNextObject():
            return None
    
        if not self.lstOutputPort[iIndex].isLinked():
            return None
    
        return self.lstOutputPort[iIndex].getLinkedPort().parentWidgetPtr()


    
    def getName(self):
        return self.name

    # For walking though the schema. For blocks, next and previous objects are other blocks.
    def getNbPreviousObject(self):
        return len(self.inputPorts)
        
    def getNbNextObject(self) :      
        return len(self.lstOutputPort)
        
        
    
    ###########################################################################
    # EVENTS 
    ###########################################################################


    # Events overloading
    def mouseDoubleClickEvent(self, qMouseEvent):
        if qMouseEvent.button() == QtCore.Qt.LeftButton:
            print "double click"
    
        super(ProcessBlock, self).mouseDoubleClickEvent(qMouseEvent)


    """    
    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton :
            self.bDragging = True
            self.lastMousePos = event.globalPos()


    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.bDragging = False;
    
    """
    def mouseMoveEvent(self, qGraphicsSceneMouseEvent) :
        super(ProcessBlock, self).mouseMoveEvent(qGraphicsSceneMouseEvent)        
        for port in self.inputPorts.itervalues():
            port.updateLinksPosition()    

        for port in self.outputPorts.itervalues():
            port.updateLinksPosition()         
       
            
        
    """
    
    def mouseMoveEvent(self, event):
        
        if self.bDragging:   # The block is being moved.
        
            self.move(self.pos() + event.globalPos() - self.lastMousePos)

            for port in self.inputPorts.itervalues():
                port.changeLinkP2(port.mapTo(self.parentWidget(), port.linkingPoint) + event.globalPos() - self.lastMousePos)
                
    
            for port in self.outputPorts.itervalues():
                port.changeLinkP1(port.mapTo(self.parentWidget(), port.linkingPoint) + event.globalPos() - self.lastMousePos)
    
            self.lastMousePos = event.globalPos() 
            #self.schemaObjectMoved.emit()
    
    """    


    def focusInEvent(self, event):
        self.setResizeVisible(True)
        self.selected = True
        self.blockFocusInEvent.emit(self.block)
        super(ProcessBlock, self).focusInEvent(event)
        self.update()

    
    def focusOutEvent(self, event):
        if self.viewHasFocus:
            self.setResizeVisible(False)
            self.selected = False
            self.blockFocusOutEvent.emit()
            super(ProcessBlock, self).focusInEvent(event)
            self.update()

    
    """
    def closeEvent(self, event):
        for port in self.outputPorts.itervalues():
            for link in port.links:
                link.close()
    """ 

    
    def resizeEvent(self, event):
        pass
    
    def geometryChanged(self):
        self.resizePointsTop.setPos(        self.width/2, 0)
        self.resizePointsBottom.setPos(     self.width/2, self.height-self.resizePointsBottom.height)
        self.resizePointsLeft.setPos(       0,         self.height/2)
        self.resizePointsRight.setPos(      self.width-self.resizePointsRight.width,   self.height/2)
        self.resizePointsTopRight.setPos(   self.width-self.resizePointsTopRight.width,   0)
        self.resizePointsTopLeft.setPos(    0,         0)
        self.resizePointsBottomRight.setPos(self.width-self.resizePointsBottomRight.width,   self.height-self.resizePointsBottomRight.height)
        self.resizePointsBottomLeft.setPos( 0,         self.height-self.resizePointsBottomLeft.height)
    
    
        lblRect = self.lblBox.boundingRect()    
        self.lblBox.setPos(self.width/2-lblRect.width()/2 + PORT_SIZE/2, self.height/2-lblRect.height()/2)
    
        nbItem = len(self.inputPorts)
        for i2, port in enumerate(self.inputPorts.itervalues()):
            port.setPos(PORT_SIZE, self.height*(i2+1)/(nbItem+1) - PORT_SIZE/2)
            port.updateLinksPosition()    

        nbItem = len(self.outputPorts)
        for i2, port in enumerate(self.outputPorts.itervalues()):
            port.setPos(self.width-PORT_SIZE, self.height*(i2+1)/(nbItem+1) - PORT_SIZE/2)
            port.updateLinksPosition()        
    
      
        
