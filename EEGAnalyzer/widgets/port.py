# -*- coding: utf-8 -*-
"""
Created on Thu Jul 11 17:03:25 2013

@author: Christian
"""

from PySide.QtCore import *
from PySide.QtGui import *

from copy import deepcopy

from schemaobject import SchemaObject
from constant import PORT_SIZE
from link import ArrowLink

__PORT_TYPE_OUTPUT__  = 0x0000
__PORT_TYPE_INPUT__   = 0x0001



class Port(SchemaObject):

    # Is there currently a drag-and-drop in operation from a Port object
    inDragAndDrop = False

    updateEvent = Signal(QRegion)
    
    def __init__(self, name, parent, portType):
        
        super(Port, self).__init__(parent)

        self.leftMouseButtonDown = False

        # Is a candidate (if the mouse button is release before the
        # cursor go out of reach)
        # to receive the drop from a drag and drop.
        self.isDoppingCandidate = False
    
        # Port link object
        self.__links = []
    
        self.linkedPorts = []
    
        # Name of the port
        self.name = name
    
        # Last recorded position of the mouse
        self.lastMousePos = None
    
        # Variables defining the shape of the port
        self.portPoly = QPolygon([QPoint(0, 0), QPoint(PORT_SIZE, PORT_SIZE/2), QPoint(0, PORT_SIZE)])
        
    
        self.portType = portType;

        if self.portType & __PORT_TYPE_INPUT__:
            self.linkingPoint = QPoint(0, PORT_SIZE/2);
        else:
            self.linkingPoint = QPoint(PORT_SIZE, PORT_SIZE/2);


        self.setAcceptDrops(True)
        #self.setAutoFillBackground(True)
        #self.setBackgroundRole(QPalette.Window)
        #self.setFocusPolicy(Qt.StrongFocus);
        
        #mask = QBitmap(parent.size())
        #mask.clear()
        #painter = QPainter()
        #painter.begin(mask)
        #painter.setPen(Qt.color1)
        #painter.setBrush(Qt.color1)
        #painter.drawConvexPolygon(self.portPoly)
        #painter.end()
        #self.setMask(mask)
    
    

    
    
    @property
    def linkingPoint(self):
        return self._linkingPoint
        
    @linkingPoint.setter
    def linkingPoint(self, linkingPoint):  
        self._linkingPoint = linkingPoint    
        self.updateLinksPosition()    
        

    def updateLinksPosition(self):
        for link in self.__links:
            if self.isInput() :
                link.p1 = self.mapToScene(self._linkingPoint)
            elif self.isOutput():
                link.p2 = self.mapToScene(self._linkingPoint)     
            else:
                raise ValueError("The port should be either an input or an output port.")        
        

    """
    
    @property
    def links(self):
        return self.__links
        
    @links.setter
    def links(self, links):    
        self.__links = links
        self.links.updateEvent.connect(self.emitUpdateEvent)
    """

        
    def addLink(self, link):    
        self.__links.append(link)
        link.updateEvent.connect(self.emitUpdateEvent)
        
        if self.isInput() :
            link.p1 = self.mapToScene(self.linkingPoint)
        elif self.isOutput():
            link.p2 = self.mapToScene(self.linkingPoint)     
        else:
            raise ValueError("The port should be either an input or an output port.")
        
        
    
    def emitUpdateEvent(self, region):
         self.updateEvent.emit(region)
    
        
    def changeLinkP2(self, point):
        for link in self.links:
            link.p2 = point

    
    
    def changeLinkP1(self, point):
        for link in self.links:
            link.p1 = point


    
    
    
    
    
    def getLinkedPortNames(self):
        return [self.linkedPort.getPortName()]
        if self.linkedPort:
            return self.linkedPort.getPortName()
        return ""
    


    # For walking though the schema. For ports, these functions return arrow linked to them.
    def getNbPreviousObject(self) : return 1 if self.isLinked() and self.isInput()  else 0
    def getNbNextObject(self)     : return 1 if seld.isLinked() and self.isOutput() else 0
    def previousObject(self)      : return self.getLinkPtr() if self.getNbPreviousObject() else None
    def nextObject(self)          : return self.getLinkPtr() if self.getNbNextObject()     else None


    def getPortType(self)         : return self.portType
    def isInput(self)             : return self.portType and __PORT_TYPE_INPUT__
    def isOutput(self)            : return not self.isInput()
    def getPortName(self)         : return self.strName

    def getLink(self)             : return self.link
    def isLinked(self)            : return self.link != None
    def getLinkedPort(self)       : return self.linkedPort



    #private slots:
    #    virtual void formatObjectCode() {};


    def boundingRect(self):
        return self.parentItem().rect

    def paint(self, painter, option, widget):
        
        painter.setBrush(QBrush(QColor(0, 0, 0)))                        
        painter.drawConvexPolygon(self.portPoly)
        
        
        #self.setAutoFillBackground(True)
        #self.setBackgroundRole(QPalette.Window)
        #self.setFocusPolicy(Qt.StrongFocus);
        
        #mask = QBitmap(parent.size())
        #mask.clear()
        #painter = QPainter()
        #painter.begin(mask)
        #painter.setPen(Qt.color1)
        #painter.setBrush(Qt.color1)
        #painter.drawConvexPolygon(self.portPoly)
        #painter.end()
        #self.setMask(mask)
            
        
        """
    
    def paintEvent(self, event) :
        Pal = self.palette()
        # set black background
        if self.hasFocus() or self.isDoppingCandidate :
            Pal.setColor(QPalette.Background, Qt.red)
        else :
            Pal.setColor(QPalette.Background, Qt.black)
        self.setPalette(Pal)
        QWidget.paintEvent(self, event)
   
    """   
   
    """
    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton :
            self.leftMouseButtonDown = True
            self.lastMousePos        = event.globalPos()
    
    
    def mouseReleaseEvent(self, event) :
        if event.button() == Qt.LeftButton:
            Port.inDragAndDrop         = False
            self.leftMouseButtonDown   = False

    
    def mouseMoveEvent(self, event) :
        if self.leftMouseButtonDown :
            if not Port.inDragAndDrop :
                # TODO: Double call to parent might be dangerous. Might be better to use
                # events.
                
                link = ArrowLink()
                self.scene().addItem(link)
                self.links.append(link) # the caneva is the parent
    
                if self.portType & __PORT_TYPE_INPUT__:
                    link.p2 = self.mapToScene(self.linkingPoint)
                else:
                    link.p1 = self.mapToScene(self.linkingPoint)
    
                #holder.creatingALink(link, self)
    
                #drag     = QDrag(self)
                mimeData =  QMimeData()
                mimeData.setText("")
    
                #drag.setMimeData(mimeData)
    
                #Port.inDragAndDrop = True
                #drag.exec_()
                #Port.inDragAndDrop = False
    
    
    
    
    def dragEnterEvent(self, event):
        if Port.inDragAndDrop:
            # TODO: Double call to parent might be dangerous. Might be better to use
            # events.
            holder          = self.getHolder()
            pInitiatingPort = holder.getLinkInitPort()
    
            # The initiating port and the receiving port for the connection must be
            # complementary (one input, one output)
            if (self.portType & __PORT_TYPE_INPUT__ ) != (pInitiatingPort.getPortType() & __PORT_TYPE_INPUT__) :
                link  = pInitiatingPort.getLink()
    
                self.isDoppingCandidate = True
                event.acceptProposedAction()
    
                if self.portType & __PORT_TYPE_INPUT__ :
                    link.setP2(self.mapTo(holder, self.linkingPoint))
                else:
                    link.setP1(self.mapTo(holder, self.linkingPoint))
    
                #link.redefine()
    
                self.update()

    
    
    def dragLeaveEvent(self, event):
        self.isDoppingCandidate = False
        self.update()
    
    
    
    def dropEvent(self, event):
    
        if Port.inDragAndDrop :
            # TODO: Double call to parent might be dangerous. Might be better to use
            # events.
            holder = self.getHolder()
    
            self.linkedPort = holder.getLinkInitPort()
            
            # The initiating port and the receiving port for the connection must be
            # complementary (one input, one output)
            if (self.portType & __PORT_TYPE_INPUT__) != (self.linkedPort.getPortType() & __PORT_TYPE_INPUT__) :
                self.linkedPort.linkedPort = self
                self.setLink(self.linkedPort.getLink())
    
                holder.finishingALink(self)
                event.acceptProposedAction()

    
    def moveEvent(self, event):

        for link in self.links:
            oldP1 = deepcopy(link.p1)
            oldP2 = deepcopy(link.p2)            
            
            if self.portType & __PORT_TYPE_INPUT__:
                link.p2 = self.mapTo(self.getHolder(), self.linkingPoint)# - event.oldPos() + event.pos()
            else :
                link.p1 = self.mapTo(self.getHolder(), self.linkingPoint)# - event.oldPos() + event.pos()
    
            link.redefine(oldP1, oldP2)
    """