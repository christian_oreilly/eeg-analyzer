# -*- coding: utf-8 -*-
"""
Created on Wed Nov 13 19:39:48 2013

@author: oreichri
"""

from PySide import QtGui, QtCore
from PyBlockWork.registerWrapper import RegisterWrapper
import pandas as pd


class RegisterDialog(QtGui.QDialog):

    def __init__(self, parent, registerName, HMAC_KEY):
        super(RegisterDialog, self).__init__(parent)
        self.setWindowTitle("Register content");
        self.registerView = RegisterView(self, registerName, HMAC_KEY)
        self.registerView.close.connect(self.close)

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.registerView)
        self.setLayout(layout)






class RegisterView(QtGui.QWidget):
    
    close = QtCore.Signal()  
    
    def __init__(self, parent, registerName, HMAC_KEY) :
        super(RegisterView, self).__init__(parent)

        # Defaultspace between blocks when using automatic placement
        self.registerName       = registerName
        self.HMAC_KEY           = HMAC_KEY

        #self.listWidget         = QtGui.QListWidget(self)
        self.refreshBtn         = QtGui.QPushButton("Refresh", self)
        self.refreshBtn.clicked.connect(self.refresh)

        self.createTable() 

        layout         = QtGui.QVBoxLayout()
        layout.addWidget(QtGui.QLabel("Registered blocks"))
        #layout.addWidget(self.listWidget)
        layout.addWidget(self.tableView) 
        layout.addWidget(self.refreshBtn)
             
        self.setLayout(layout)
        self.refresh()



    def refresh(self):
        registerWrap = RegisterWrapper(self.registerName)
        if not registerWrap.isValid():  
            QtGui.QMessageBox.critical(self, "Error connecting to the register", 
                 "Is the schema running? If the schema is not running, the register"\
                 " is not online and thus its content cannot be displayed.")
            self.close.emit()
            return
        
        register = eval(registerWrap.register)
        self.tableModel.setTableData(register)
        
        #self.listWidget.clear()
        #
        #for ID, string in register.iteritems():
        #    self.listWidget.addItem(ID + ": " + string)

                
        
    def createTable(self):
        # create the view
        self.tableView = QtGui.QTableView()

        # set the table model
        self.tableModel = RegisterTableModel(self) 
        self.tableView.setModel(self.tableModel)

        # set the minimum size
        self.tableView.setMinimumSize(400, 300)

        # hide grid
        self.tableView.setShowGrid(False)

        # set the font
        font = QtGui.QFont("Courier New", 8)
        self.tableView.setFont(font)

        # hide vertical header
        vh = self.tableView.verticalHeader()
        vh.setVisible(False)

        # set horizontal header properties
        hh = self.tableView.horizontalHeader()
        hh.setStretchLastSection(True)

        # set column width to fit contents
        self.tableView.resizeColumnsToContents()

        # set row height
        for row in xrange(self.tableModel.rowCount()):
            self.tableView.setRowHeight(row, 18)

        # enable sorting
        self.tableView.setSortingEnabled(True)


        







class RegisterTableModel(QtCore.QAbstractTableModel): 
    def __init__(self, parent=None, *args): 
        """ datain: a list of lists
            headerdata: a list of strings
        """
        QtCore.QAbstractTableModel.__init__(self, parent, *args) 
        
        self.modelData   = pd.DataFrame()     
        self.showCol     = {}
        self.visibleCols = []
        
    def rowCount(self, parent=None): 
        return self.modelData.shape[0]
 
    def columnCount(self, parent=None): 
        return sum(self.showCol.values())
 
 
    def data(self, index, role):
        if self.columnCount() == 0 or not index.isValid():
            return None       
        
        if role != QtCore.Qt.DisplayRole: 
            return None
            
        return self.modelData.ix[index.row(), self.visibleCols[index.column()]] 



    def headerData(self, col, orientation, role):
        colName = self.visibleCols[col]
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole and self.showCol[colName]:
            return colName
        return None


    def sort(self, Ncol, order):
        """Sort table by given column number.
        """
        if self.columnCount() == 0:
            return

        #self.layoutAboutToBeChanged.emit()
        
        if order == QtCore.Qt.DescendingOrder:
            ascending  = False           
        else:
            ascending  = True            
            
        self.modelData.sort(self.visibleCols[Ncol], ascending=ascending, inplace=True)
        self.modelData.reset_index(drop=True, inplace=True)            
            
        topLeft     = self.createIndex(0, 0) 
        bottomRight = self.createIndex(self.rowCount()-1, self.columnCount()-1)
        self.dataChanged.emit(topLeft, bottomRight)

        #self.layoutChanged.emit()
        


    def setTableData(self, register):

        IDs             = []
        hasExecuteds    = []
        hasStarteds     = []
        blockStrings    = []
        inputs          = []
        outputs         = []
        inputs          = []
        names           = []
        nextBlockIDs    = []
        previousBlockIDs= []
        depth           = []
        
        
        for ID, string in register.iteritems():
            registerItemDict = eval(string) 
            IDs.append(ID)
            hasExecuteds.append(str(registerItemDict["hasExecuted"]))
            hasStarteds.append(str(registerItemDict["hasStarted"]))
            block = eval(registerItemDict["blockString"])
            inputs.append(str(block["inputs"]))
            outputs.append(str(block["outputs"]))
            names.append(str(block["name"]))
            depth.append(str(block["depth"]))
            nextBlockIDs.append(str(block["nextBlockIDs"]))
            previousBlockIDs.append(str(block["previousBlockIDs"]))
            
            del block["inputs"]
            del block["outputs"]
            del block["name"]
            del block["nextBlockIDs"]
            del block["previousBlockIDs"]
            del block["depth"]
            blockStrings.append(str(block))
            
            
        
        self.layoutAboutToBeChanged.emit()        
        self.modelData = pd.DataFrame({"ID"             : IDs, 
                                      "name"            : names,
                                      "depth"           : depth, 
                                      "next blocks"     : nextBlockIDs,
                                      "previous blocks" : previousBlockIDs,
                                      "executed"        : hasExecuteds,
                                      "started"         : hasStarteds,
                                      "inputs"          : inputs,
                                      "outputs"         : outputs,
                                      "blockString"     : blockStrings})   
                      
        self.showCol = {"ID"                : True, 
                        "name"              : True, 
                        "depth"             : True, 
                        "next blocks"       : True, 
                        "previous blocks"   : True, 
                        "executed"          : True,
                        "started"           : True,
                        "inputs"            : True,
                        "outputs"           : True,
                        "blockString"       : True}   
 
        self.visibleCols = [name for name in self.modelData.columns.tolist() if self.showCol[name]]                            

        self.layoutChanged.emit()


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        
        
        
        
    












        