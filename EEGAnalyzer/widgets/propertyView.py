# -*- coding: utf-8 -*-
"""
Created on Thu Nov 07 20:18:47 2013

@author: Christian
"""


from PySide import QtCore, QtGui

import pandas as pd
import datetime
        

QtCore.Qt.EditNoEmit = QtCore.Qt.UserRole + 1


class TableModel(QtCore.QAbstractTableModel): 
    def __init__(self, parent=None, *args): 
        """ datain: a list of lists
            headerdata: a list of strings
        """
        QtCore.QAbstractTableModel.__init__(self, parent, *args) 
        
        self.modelData   = pd.DataFrame()     
        self.showCol     = {}
        self.visibleCols = []
        
    def rowCount(self, parent=None): 
        return self.modelData.shape[0]
 
    def columnCount(self, parent=None): 
        return sum(self.showCol.values())
 
 
    def data(self, index, role):
        if self.columnCount() == 0 or not index.isValid():
            return None       
        
        if role != QtCore.Qt.DisplayRole: 
            return None
            
        return self.modelData.ix[index.row(), self.visibleCols[index.column()]] 



    def headerData(self, col, orientation, role):
        colName = self.visibleCols[col]
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole and self.showCol[colName]:
            return colName
        return None


    def sort(self, Ncol, order):
        """Sort table by given column number.
        """
        if self.columnCount() == 0:
            return

        #self.layoutAboutToBeChanged.emit()
        
        if order == QtCore.Qt.DescendingOrder:
            ascending  = False           
        else:
            ascending  = True            
            
        self.modelData.sort(self.visibleCols[Ncol], ascending=ascending, inplace=True)
        self.modelData.reset_index(drop=True, inplace=True)            
            
        topLeft     = self.createIndex(0, 0) 
        bottomRight = self.createIndex(self.rowCount()-1, self.columnCount()-1)
        self.dataChanged.emit(topLeft, bottomRight)

        #self.layoutChanged.emit()
        
        
        

###############################################################################
# Events
###############################################################################
        

class EventTableModel(TableModel): 

    def setTableData(self, events, recordingStartTime):
        deltaTimes      = [e.startTime for e in events]
        startTimes      = [(recordingStartTime + datetime.timedelta(0, d)).strftime("%H:%M:%S") for d in deltaTimes]
        eventGroupNames = [e.groupName for e in events]
        eventTypes      = [e.name      for e in events]
        channels        = [e.channel   for e in events]
        self.layoutAboutToBeChanged.emit()        
        self.modelData = pd.DataFrame({"group"     : eventGroupNames, 
                                  "type"      : eventTypes,
                                  "duration"  : [str(e.duration())   for e in events],
                                  "channel"   : channels,
                                  "time start": startTimes,
                                  "delta time": deltaTimes})   
                      
        self.showCol = {"group"     : True, 
                        "type"      : True,
                        "duration"  : True,
                        "time start": True,
                        "channel"   : True,
                        "delta time": False}   
 
        self.visibleCols = [name for name in self.modelData.columns.tolist() if self.showCol[name]]                            

        self.layoutChanged.emit()

        
    def getRowTime(self, noRow):
        return self.modelData.ix[noRow, "delta time"]
        


import os


class EventWidget(QtGui.QWidget):
    
    def __init__(self, mainWindow, parent = None):
        super(EventWidget, self).__init__(parent)
        self.mainWindow = mainWindow

        self.setLayout(QtGui.QVBoxLayout())  
        
        self.createTable() 
        
        self.headers = self.eventTableView.horizontalHeader()
        self.headers.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.headers.customContextMenuRequested.connect(self.header_popup)        
        

        self.layout().addWidget(self.eventTableView) 

        self.eventTableView.doubleClicked.connect(self.tableDoubleClicked)


    def tableDoubleClicked(self, indexObj):
        rowTime = self.eventTableModel.getRowTime(indexObj.row())
        self.mainWindow.goToTimeDelta(rowTime)


        
        
    def header_popup(self, pos):
        menu = QtGui.QMenu()
        quitAction = menu.addAction("Quit")
        action = menu.exec_(self.mapToGlobal(pos))
        if action == quitAction:
            QtCore.qApp.quit()        
        

    def popup(self, pos):
        for i in self.tv.selectionModel().selection().indexes():
            print i.row(), i.column()
        menu = QtGui.QMenu()
        quitAction = menu.addAction("Quit")
        action = menu.exec_(self.mapToGlobal(pos))
        if action == quitAction:
            QtCore.qApp.quit()
        
        
        
    def setEventData(self, events, recordingStartTime):
        self.eventTableModel.setTableData(events, recordingStartTime)

    def createTable(self):
        # create the view
        self.eventTableView = QtGui.QTableView()

        # set the table model
        self.eventTableModel = EventTableModel(self) 
        self.eventTableView.setModel(self.eventTableModel)

        # set the minimum size
        self.eventTableView.setMinimumSize(400, 300)

        # hide grid
        self.eventTableView.setShowGrid(False)

        # set the font
        font = QtGui.QFont("Courier New", 8)
        self.eventTableView.setFont(font)

        # hide vertical header
        vh = self.eventTableView.verticalHeader()
        vh.setVisible(False)

        # set horizontal header properties
        hh = self.eventTableView.horizontalHeader()
        hh.setStretchLastSection(True)

        # set column width to fit contents
        self.eventTableView.resizeColumnsToContents()

        # set row height
        for row in xrange(self.eventTableModel.rowCount()):
            self.eventTableView.setRowHeight(row, 18)

        # enable sorting
        self.eventTableView.setSortingEnabled(True)


        











###############################################################################
# Class for selecting event groups and types
###############################################################################
        

class PropertyWidget(QtGui.QWidget):
    

    #useChanged = Signal(str, bool) 
    useChangedAll = QtCore.Signal(dict)    
    
    def __init__(self, parent = None):
        super(PropertyWidget, self).__init__(parent)

        self.setLayout(QtGui.QVBoxLayout())  
        
        self.createTable() 
        
        self.headers = self.eventTableView.horizontalHeader()
        self.headers.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.headers.customContextMenuRequested.connect(self.header_popup)        
        
        self.eventTableView.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.eventTableView.customContextMenuRequested.connect(self.popup)        
        
        self.layout().addWidget(self.eventTableView) 

        
        
    def filter(self):  
        names = self.eventTableModel.modelData["name"]
        uses  = self.eventTableModel.modelData["use"]    
        self.useChangedAll.emit(dict((name, use) for use, name in zip(uses, names)))
            
        
        
        
    def header_popup(self, pos):
        menu = QtGui.QMenu()
        quitAction = menu.addAction("Quit")
        action = menu.exec_(self.mapToGlobal(pos))
        if action == quitAction:
            QtCore.qApp.quit()        
        

    def popup(self, pos):
        menu = QtGui.QMenu()
        setColorAction = menu.addAction("Set color")
        action = menu.exec_(self.mapToGlobal(pos))
        if action == setColorAction:
            row = self.eventTableView.indexAt(pos).row()
            if row != -1:
                color = QtGui.QColorDialog.getColor(QtCore.QColor("black"), self)
                eventName = self.eventTableModel.modelData.ix[row, "name"]        
                self.mainWindow.eventColorDict[eventName] =  color               
                print color, eventName                
                
        
        
        
    def setTableData(self, properties):
        self.eventTableModel.setTableData(properties)    
        self.eventTableView.setDelegates(self.eventTableModel)




    def createTable(self):
        # create the view
        self.eventTableView = TableView()

        # set the table model
        self.eventTableModel = PropertyTableModel(self) 
        #self.eventTableModel.useChanged.connect(self.changeEventTypeDisplay)        
        
        self.eventTableView.setModel(self.eventTableModel)

        # set the minimum size
        self.eventTableView.setMinimumSize(400, 300)

        # hide grid
        self.eventTableView.setShowGrid(False)

        # set the font
        font = QtGui.QFont("Courier New", 8)
        self.eventTableView.setFont(font)

        # hide vertical header
        vh = self.eventTableView.verticalHeader()
        vh.setVisible(False)

        # set horizontal header properties
        hh = self.eventTableView.horizontalHeader()
        hh.setStretchLastSection(True)

        # set column width to fit contents
        self.eventTableView.resizeColumnsToContents()

        # set row height
        for row in xrange(self.eventTableModel.rowCount()):
            self.eventTableView.setRowHeight(row, 18)

        # enable sorting
        self.eventTableView.setSortingEnabled(True)


    #def changeEventTypeDisplay(self, name, display):     
    #    self.useChanged.emit(name, display)









class PropertyTableModel(TableModel): 
    

    useChanged    = QtCore.Signal(str, bool)
    useChangedAll = QtCore.Signal(str, bool)
    
    def setTableData(self, data):    
        
        self.layoutAboutToBeChanged.emit()            
        
        self.modelData = pd.DataFrame({"name"   : data.keys() ,
                                       "value"  : data.values()}   ) 
        self.showCol = {"name"   : True,
                        "value"  : True}      
        self.typeDel = {"name"   : "text", 
                        "value"  : "text"}
            
        self.modelData.sort("name", inplace=True)
        self.modelData.reset_index(drop=True, inplace=True)           
                                    
        self.visibleCols = [name for name in self.modelData.columns.tolist() if self.showCol[name]]                           
                        
        self.layoutChanged.emit()



    def flags(self, index):
        colName = self.modelData.columns[index.column()]
        if colName == "value":   
            return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled
        else:
            return QtCore.Qt.ItemIsEnabled


    def setData(self, index, value, role):
       
        colName = self.modelData.columns[index.column()]
        if self.typeDel[colName] == "checkbox":
            if role == QtCore.Qt.EditRole:
                self.modelData.ix[index.row(), colName] = value
                self.dataChanged.emit(index, index)  
            elif role == QtCore.Qt.EditNoEmit:
                self.modelData.ix[index.row(), colName] = value
            else:
                return False
                          
            
            #if colName == "use" and role == Qt.EditRole:
            #    self.useChanged.emit(self.modelData.ix[index.row(), "name"], value)
            
            return True
        return False


    def data(self, index, role): 
        if not index.isValid() or self.columnCount() == 0:
            return None
            
        if not self.showCol[self.modelData.columns[index.column()]]:
            return None     

        if role == QtCore.Qt.EditRole: 
            checked = QtCore.Qt.Checked if self.modelData.iloc[index.row(), index.column()] else QtCore.Qt.Unchecked
            return checked  
        
        if role == QtCore.Qt.DisplayRole: 
            if self.typeDel[self.modelData.columns[index.column()]] != "checkbox":      
                return str(self.modelData.iloc[index.row(), index.column()]) 

        return None





       
class TableView(QtGui.QTableView):

    def __init__(self, *args, **kwargs):
        QtGui.QTableView.__init__(self, *args, **kwargs)
        
        self.setEditTriggers(QtGui.QAbstractItemView.AllEditTriggers)

        
    def setDelegates(self, model):
        for i, colname in enumerate(model.modelData.columns) :
            if model.typeDel[colname] == "checkbox":
                self.setItemDelegateForColumn(i, CheckBoxDelegate(self))
                for row in range(model.rowCount()):
                    self.openPersistentEditor(model.index(row, i))




class DVCheckBox(QtGui.QCheckBox):

    customStateChanged = QtCore.Signal(int, QtCore.QModelIndex)
    customClicked      = QtCore.Signal(int, QtCore.QModelIndex)

    def __init__(self, parent, index):
        QtGui.QCheckBox.__init__(self, parent)
        
        self.index = index    
        
        #self.stateChanged.connect(self.relaunchStateChanged)
        self.clicked.connect(self.relaunchClicked)
        
    #def relaunchStateChanged(self, state):
    #    self.customStateChanged.emit(state, self.index)
        
    def relaunchClicked(self, state):  
        self.customClicked.emit(self.checkState(), self.index)






class CheckBoxDelegate(QtGui.QStyledItemDelegate):
    def __init__(self, parent):
        QtGui.QStyledItemDelegate.__init__(self, parent)

    def createEditor(self, parent, option, index):
        checkbox = DVCheckBox(parent, index)    
        checkbox.setChecked(QtCore.Qt.Checked)
        checkbox.customClicked.connect(self.checkboxCustomClicked)
        return checkbox


    def checkboxCustomClicked(self, state, index):
        value = state == QtCore.Qt.Checked     
        index.model().setData(index, value, QtCore.Qt.EditNoEmit)
        
    def editorEvent(event, model, option, index):
        return True
        
    def setEditorData(self, editor, index):
        #blocked = editor.signalsBlocked()
        #editor.blockSignals(True)
        editor.setChecked(index.model().data(index, QtCore.Qt.EditRole))
        #editor.blockSignals(blocked)        
        
        
    def setModelData(self, editor, model, index):
        pass
        

        #QStyledItemDelegate.setEditorData(self, editor, index)
   
   
"""
     int value = index.model()->data(index, Qt::EditRole).toInt();

     QSpinBox *spinBox = static_cast<QSpinBox*>(editor);
     spinBox->setValue(value);
 }
"""
        