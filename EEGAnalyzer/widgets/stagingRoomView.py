# -*- coding: utf-8 -*-
"""
Created on Wed Nov 13 19:39:48 2013

@author: oreichri
"""

from PySide import QtGui
from PyBlockWork.block import StagingRoomWrapper



class StagingRoomDialog(QtGui.QDialog):

    def __init__(self, parent, stagingRoomName, HMAC_KEY):
        super(StagingRoomDialog, self).__init__(parent)
        self.setWindowTitle("Staging room content");
        self.stagingRoomView = StagingRoomView(self, stagingRoomName, HMAC_KEY)

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.stagingRoomView)
        self.setLayout(layout)




class StagingRoomView(QtGui.QWidget):
    def __init__(self, parent, stagingRoomName, HMAC_KEY) :
        super(StagingRoomView, self).__init__(parent)

        # Defaultspace between blocks when using automatic placement
        self.stagingRoomName    = stagingRoomName
        self.HMAC_KEY           = HMAC_KEY

        self.listQueue          = QtGui.QListWidget(self)
        self.refreshBtn         = QtGui.QPushButton("Refresh", self)
        self.refreshBtn.clicked.connect(self.refresh)

        layout         = QtGui.QVBoxLayout()
        layout.addWidget(QtGui.QLabel("Waiting queue"))
        layout.addWidget(self.listQueue)
        layout.addWidget(self.refreshBtn)
             
        self.setLayout(layout)
        self.refresh()




    def refresh(self):
        stagingRoom = StagingRoomWrapper(self.stagingRoomName)
        self.listQueue.clear()
        for ID in stagingRoom.waitingBlocks:
            self.listQueue.addItem(ID)
        
        
        