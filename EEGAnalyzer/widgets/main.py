# -*- coding: utf-8 -*-
"""
Created on Thu Jul 11 13:45:27 2013

@author: Christian
"""

import sys
from PySide.QtCore import *
from PySide.QtGui import *

from mainwindow import MainWindow


#include <QtWidgets/QApplication>
#include "mainwindow.h"
   

if __name__ == "__main__":
    
    app     = QApplication(sys.argv)
    myApp   = MainWindow()
    myApp.show()
    
    sys.exit(app.exec_())

                       
