# -*- coding: utf-8 -*-
"""
Created on Thu Jul 11 15:36:10 2013

@author: Christian
"""


from PySide.QtCore import *
from PySide.QtGui import *
from PySide import QtCore

from constant import PORT_SIZE
from schemaobject import SchemaObject

#include "global.h"

SIZEPOINT_TOP    = 1
SIZEPOINT_BOTTOM = 2
SIZEPOINT_LEFT   = 4
SIZEPOINT_RIGHT  = 8


class SizePoint(SchemaObject):#QWidget): 

    def __init__(self, parent, typePoint):
        super(SizePoint, self).__init__(parent)

        #Pal = self.palette()
        #Pal.setColor(QPalette.Window, Qt.red)
        #self.setPalette(Pal)
        #self.setBackgroundRole(QPalette.Window)
        #self.setAutoFillBackground(True)
        
        # Are we currently in a drag-and-drop operation
        self.bDragging = False

        # Last recorded position of the mouse
        self.lastMousePos = None

        # What kind of size point is it.
        self.typePoint = typePoint;

        self.rect =  QRectF(0,0,4, 4)
        self.setVisible(False)
    
        if self.typePoint & SIZEPOINT_TOP:
            if self.typePoint & SIZEPOINT_LEFT:
                self.setCursor(Qt.SizeFDiagCursor)
            elif self.typePoint & SIZEPOINT_RIGHT:
                self.setCursor(Qt.SizeBDiagCursor)
            else : 
                self.setCursor(Qt.SizeVerCursor);

        elif self.typePoint & SIZEPOINT_BOTTOM:
            if self.typePoint & SIZEPOINT_LEFT:
                self.setCursor(Qt.SizeBDiagCursor )
            elif self.typePoint & SIZEPOINT_RIGHT:
                self.setCursor(Qt.SizeFDiagCursor )
            else:
               self.setCursor(Qt.SizeVerCursor)
               
        elif self.typePoint & SIZEPOINT_LEFT:
            self.setCursor(Qt.SizeHorCursor)
            
        elif self.typePoint & SIZEPOINT_RIGHT:
            self.setCursor(Qt.SizeHorCursor )

    
    
    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.bDragging      = True
            self.lastMousePos   = event.globalPos()


    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.bDragging = False
    
    
    
    def mouseMoveEvent(self, event):
    
        if self.bDragging: 
            #self.lastMousePos = self.mapToGlobal(QPoint(2,2))
            #QApplication.desktop().cursor().setPos(self.lastMousePos)
            self.lastMousePos = event.lastScreenPos()
    
            oldSize     = self.parentWidget().size()
            delta       = event.scenePos() - self.lastMousePos
            deltaSize   = QSize(delta.x(), delta.y())    
    
            # We dont change size in direction not allowed by the SizePoint object.
            if not (self.typePoint & SIZEPOINT_TOP or self.typePoint & SIZEPOINT_BOTTOM) :
                deltaSize.setHeight(0)
    
            if not (self.typePoint & SIZEPOINT_LEFT or self.typePoint & SIZEPOINT_RIGHT) :
                deltaSize.setWidth(0);
    
            if self.typePoint & SIZEPOINT_TOP:
                deltaSize.setHeight(-deltaSize.height())
    
            if self.typePoint & SIZEPOINT_LEFT:
                deltaSize.setWidth(-deltaSize.width())
    
    
            # We make sure to keep a minimum size of 4*PORT_SIZE X 4*PORT_SIZE
            newSize = deltaSize + self.parentWidget().size()
            if newSize.width() < 4*PORT_SIZE :
                newSize.setWidth(4*PORT_SIZE)
                delta.setX(( 1 if delta.x() > 0 else -1 )*(newSize - oldSize).width())
    

    
            if newSize.height() < 4*PORT_SIZE:
                newSize.setHeight(4*PORT_SIZE)
                delta.setY(( 1 if delta.y() > 0 else -1 )*(newSize - oldSize).height())
    
            self.lastMousePos += delta;
    

            QApplication.desktop().cursor().setPos(self.lastMousePos)

    
            blockPosition = self.parentWidget().pos()
            # If needed, we move the ProcessBlock so as to cancel
            # a moving illustion that would be produced by the resizing
            # of the block with a constant position at the upper left corner.
            if self.typePoint & SIZEPOINT_TOP :
                blockPosition.setY(blockPosition.y() - (newSize - oldSize).height() )
    
            if self.typePoint & SIZEPOINT_LEFT :
             blockPosition.setX(blockPosition.x() - (newSize - oldSize).width() )
    
            self.parentWidget().move(blockPosition)
    
            # We resize.
            self.parentWidget().resize(newSize)




    def boundingRect(self):
        return self.rect


    def paint(self, painter, option, widget):
        pen = QPen()
        pen.setColor(QtCore.Qt.red)
        pen.setWidth(1)
        painter.setPen(pen)
        painter.setBrush(QBrush(QColor(255, 0, 0)))     
        painter.drawRect(self.rect)


    @property
    def x(self):
        return self.rect.x()

    @x.setter
    def x(self, x):
        return self.rect.setX(x)
        

    @property
    def y(self):
        return self.rect.y()

    @y.setter
    def y(self, y):
        return self.rect.setY(y)
        
        
    
    

    @property
    def width(self):
        return self.rect.width()

    @property
    def height(self):
        return self.rect.height()
