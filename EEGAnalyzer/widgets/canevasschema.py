# -*- coding: utf-8 -*-
"""
Created on Thu Jul 11 14:02:55 2013

@author: Christian
"""


from PySide.QtCore import *
from PySide.QtGui import *
from PySide import QtGui


from port import __PORT_TYPE_INPUT__


class ZoomGraphicsView(QGraphicsView):

    viewGotFocus = Signal()
    viewLostFocus = Signal()
    
    def wheelEvent(self, qWheelEvent):
        if qWheelEvent.modifiers() & Qt.ControlModifier  :  # zoom only when CTRL key pressed
            numSteps = qWheelEvent.delta() / 15.0 / 8.0
     
            if numSteps == 0:
                qWheelEvent.ignore()
                return

            sc = 1.25**numSteps   # I use scale factor 1.25
            self.zoom(sc, self.mapToScene(qWheelEvent.pos()))
            qWheelEvent.accept()
        else:
            super(ZoomGraphicsView, self).wheelEvent(qWheelEvent)

     
    def zoom(self, factor, centerPoint):
        self.scale(factor, factor)
        self.centerOn(centerPoint)


    def focusInEvent(self, qFocusEvent):
        self.viewGotFocus.emit()

    def focusOutEvent(self, qFocusEvent):
        self.viewLostFocus.emit()



class CanevasSchema(QFrame): #(SchemaObject):

    # SIGNALS
    
    # Emitted when the focus has changed from one object to another in the canevas.
    # This allows, for exemple, to update the object properties window. The newly
    # selected object is passed as parameter.
    selectedSchemaObjectChanged = Signal(object)

    # Emitted when the logic of the schema has changed. This can occur for many reason:
    # a block has been added/deleted, a link has been added/deleted, etc. The argument is
    # a vector of the objects for which the logic has been altered. For example, when a
    # new link is established between two blocs, the logic of two SchemaObject has been alterred.
    schemaLogicChanged          = Signal(list) 

    def __init__(self, parent=None):
        super(CanevasSchema, self).__init__(parent)
    
    
        self.scene = QGraphicsScene(self)
        self.view  = ZoomGraphicsView(self)
        self.view.setScene(self.scene)
        
        self.setLayout(QtGui.QHBoxLayout())    
        self.layout().addWidget(self.view)
        
        # Dict of ProcessBlock objects indexed by the block's names (string)        
        self.mapBlock = {}

        # True if presently creating a link between two blocks
        self.bCreatingLink = False
    
        # Link object being created
        self.link = None
    
        # Port type from the port initiating the link creation.
        self.initiatingPort = None
    
        # To be passed as parameter when the signal
        # schemaLogicChanged(...) is emmited.
        self.modifiedObjects = []
        
        self.setAcceptDrops(True)
        
        Pal = self.palette()
        # set black background
        Pal.setColor(QPalette.Background, Qt.white)
        self.setAutoFillBackground(True)
        self.setPalette(Pal)
    
        self.setFocusPolicy(Qt.StrongFocus)



    def addBlock(self, processBlock, blockName=""):
        
        # If no name proposed, we use the name of the block class,
        # which we take in lowercase to differentiate the instance (lowercase)
        # and the     std::map<string, shared_ptr<ProcessBlock> > m_mapBlock;
        if blockName == "":
            blockName = processBlock.getName().lower()

    
        # If the proposed name already exist, we append an integer
        # requential number
        tmpStr = blockName
        i      = 1
        # If the key is already in use in m_mapBlock
        while tmpStr in self.mapBlock :
            tmpStr = blockName +  str(i)
            i += 1
    
        # Storing the block inside the caneva
        self.mapBlock[tmpStr] = processBlock
    
        
        self.view.viewGotFocus.connect(processBlock.viewGotFocus)    
        self.view.viewLostFocus.connect(processBlock.viewLostFocus)        
        
        processBlock.selectedSchemaObjectChanged.connect(self.emitSelectedSchemaObjectChanged)
        processBlock.updateEvent.connect(self.update)
        processBlock.blockFocusInEvent.connect(self.blockFocusInEvent)
        processBlock.blockFocusOutEvent.connect(self.blockFocusOutEvent)
    
    
        # Store the absolute position
        #globalPos = processBlock.mapToGlobal(processBlock.pos())
    
        # Reparenting to make it being a child of the caneva.
        #processBlock.setParent(self)
    
        # Restore the absolute position to cancel the shift occuring
        # when reparenting.
        #processBlock.move(processBlock.mapFromGlobal(globalPos))
    
        # Make sure that it shows.
        #self.mapBlock[tmpStr].show()
    
        #self.modifiedObjects = []
        #self.modifiedObjects.append(processBlock)
        #emitSchemaLogicChanged();
    
        self.scene.addItem(processBlock)
        
        #blockRect = processBlock.mapRectToScene(processBlock.boundingRect())
        #self.scene.setSceneRect(0, 0, 1000, 1000)        
        
    
    def getSelectedBlock(self):
        for block in self.mapBlock.itervalues():
            if block.selected :
                return block
                
        return None
    
    
    def blockFocusInEvent(self, block):
        pass
        
        
    def blockFocusOutEvent(self):
        pass
        
    
    
    def clear(self):
        # Dict of ProcessBlock objects indexed by the block's names (string)  
        #for item in self.mapBlock.itervalues():
        #    item.close()
        self.mapBlock.clear()

        self.scene.clear()

        # True if presently creating a link between two blocks
        self.bCreatingLink = False
    
        # Link object being created
        self.link = None
    
        # Port type from the port initiating the link creation.
        self.initiatingPort = None
    
        # To be passed as parameter when the signal
        # schemaLogicChanged(...) is emmited.
        self.modifiedObjects = []
    




    def emitSelectedSchemaObjectChanged(self, objectSelected):
        self.selectedSchemaObjectChanged.emit(objectSelected)
        

    def emitSchemaLogicChanged(self):        
        self.schemaLogicChanged.emit(self.modifiedObjects)


    def finishingALink(self, receivingPort) :
        
        if receivingPort and self.initiatingPort :
            self.modifiedObjects = []
            self.modifiedObjects.append(receivingPort.parentWidget())
            self.modifiedObjects.append(self.initiatingPort.parentWidget())
            self.emitSchemaLogicChanged()
    
        self.link               = None
        self.initiatingPort     = []
        self.bCreatingLink      = False



    def creatingALink(self, link, initPort):
        self.link           = link
        self.initiatingPort = initPort
        self.bCreatingLink  = True


    def dragMoveEvent(self, event):
        if self.bCreatingLink:
            if not self.link is None :
                if self.initiatingPort.getPortType() & __PORT_TYPE_INPUT__ :
                    self.link.setP1(event.pos())
                else:
                    self.link.setP2(event.pos())
    
                #self.link.redefine()
    

    def dragEnterEvent(self, event):
        if self.bCreatingLink:
            event.acceptProposedAction()



    def isCreatingALinkself(self) : return self.bCreatingLink
    def getLinkInitPort(self)     : return self.initiatingPort

    # For walking though the schema.
    def getNbPreviousObject(self)   : return 0
    def getNbNextObject(self)       : return 0
    def previousObject(self, index) : return None
    def nextObject(self, index)     : return None


"""


class CanevasSchema : public SchemaObject
{
    Q_OBJECT
private:
    std::map<string, shared_ptr<ProcessBlock> > m_mapBlock;
    std::list<shared_ptr<Link> > lstLinks;

    // To be passed as parameter when the signal
    // schemaLogicChanged(...) is emmited.
    vector<SchemaObjectPtr> m_modifiedObjects;

    // True if presently creating a link between two blocks
    bool m_bCreatingLink;

    // Link object being created
    shared_ptr<Link> m_ptrLink;

    // Port type from the port initiating the link creation.
    PortPtr m_pInitiatingPort;

public:
    explicit CanevasSchema(QWidgetPtr parent);
    ~CanevasSchema();

    void        init() {};

    void addBlock(shared_ptr<ProcessBlock> r_ptrBlock, string r_strBlockName = "");



    void finishingALink(PortPtr r_ptrReceivingBlock);
    void creatingALink(shared_ptr<Link> r_ptrLink, PortPtr r_pInitPort);



private slots:
    virtual void formatObjectCode() {};



private slots:
    void emitSchemaLogicChanged();

"""
