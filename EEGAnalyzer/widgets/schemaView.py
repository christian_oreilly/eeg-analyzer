# -*- coding: utf-8 -*-
"""
Created on Thu Jul 11 13:51:16 2013

@author: Christian
"""

from PySide import QtGui, QtCore
import numpy as np
import Pyro4
import sys

from PyBlockWork.block import fromPrevious
from PyBlockWork.registerWrapper import RegisterWrapper
from PyBlockWork.pyroWarehouse import warehouse
from canevasschema import CanevasSchema
from processblock import ProcessBlock
from widgets.link import ArrowLink
from widgets.propertyView import PropertyWidget
from widgets.consoleWidget import ConsoleWidget
from widgets.stagingRoomView import StagingRoomDialog
from widgets.registerView import RegisterDialog
from widgets.databaseView import DatabaseDialog






class SchemaView(QtGui.QWidget):
    
    startRegisterRequest = QtCore.Signal()    
    
    
    def __init__(self, parent, schema=None, registerName=None, comHubName=None, HMAC_KEY=None,
                 stagingRoomName=None) :
        super(SchemaView, self).__init__(parent)

        # Defaultspace between blocks when using automatic placement
        self.betweenblockspace     = 20

        self.schema             = schema
        self.registerName       = registerName
        self.stagingRoomName    = stagingRoomName
        self.comHubName         = comHubName
        self.databaseName       = None
        self.HMAC_KEY           = HMAC_KEY
        self.blockInFocus       = None
        self.console            = ConsoleWidget()     
        self.drawnBlockItems    = {}
        
        self.canevas        = CanevasSchema()
        spLeft              = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, 
                                                QtGui.QSizePolicy.Preferred)
        spLeft.setHorizontalStretch(3)
        self.canevas.setSizePolicy(spLeft)

        
        self.propertyWidget = PropertyWidget()
        spRight             = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, 
                                                QtGui.QSizePolicy.Preferred)
        spRight.setHorizontalStretch(1)
        self.propertyWidget.setSizePolicy(spRight)
        

        layout         = QtGui.QHBoxLayout() #QGridLayout()
        controlsLayout = QtGui.QGridLayout()
        layout.addWidget(self.canevas) #, 0, 0)
             
             
        self.displayGroupBox    = QtGui.QGroupBox("Schema mode")
        self.canonicalBtn       = QtGui.QRadioButton("Canonical")
        self.mixedBtn           = QtGui.QRadioButton("Mixed")
        self.expandedBtn        = QtGui.QRadioButton("Expanded")
    
        self.processModeGroupBox= QtGui.QGroupBox("Processing mode")
        self.horizontalBtn      = QtGui.QRadioButton("Horizontal")
        self.verticalBtn        = QtGui.QRadioButton("Vertical")
    
    
    
        self.showStagingRoomBtn = QtGui.QPushButton("Show staging room")
        self.showRegisterBtn    = QtGui.QPushButton("Show register")    
        self.showDatabaseBtn    = QtGui.QPushButton("Show database")    
        self.updateSchemaBtn    = QtGui.QPushButton("Update schema")     
        self.rollbackBtn        = QtGui.QPushButton("Rollback select block")    
    
        self.displayGroupBox.clicked.connect(self.drawSchema)  
        self.displayGroupBox.clicked.connect(self.changeProcessMode)  
        self.showStagingRoomBtn.clicked.connect(self.showStagingRoom)
        self.showRegisterBtn.clicked.connect(self.showRegister)
        self.showDatabaseBtn.clicked.connect(self.showDatabase)
        self.updateSchemaBtn.clicked.connect(self.drawSchema)
        self.rollbackBtn.clicked.connect(self.rollback)

    
    
        self.canonicalBtn.setChecked(True)
        self.verticalBtn.setChecked(True)
    
        diplayGroupLayout = QtGui.QHBoxLayout()
        diplayGroupLayout.addWidget(self.canonicalBtn)
        diplayGroupLayout.addWidget(self.mixedBtn)
        diplayGroupLayout.addWidget(self.expandedBtn)
        
    
        processModeGroupLayout = QtGui.QHBoxLayout()
        processModeGroupLayout.addWidget(self.horizontalBtn)
        processModeGroupLayout.addWidget(self.verticalBtn)
       
        
        #vbox->addStretch(1);
        self.displayGroupBox.setLayout(diplayGroupLayout)           
        self.processModeGroupBox.setLayout(processModeGroupLayout)        
        
        controlsLayout.addWidget(self.propertyWidget,       0, 0, 1, 2)
        controlsLayout.addWidget(self.console,              1, 0, 1, 2)
        controlsLayout.addWidget(self.displayGroupBox,      2, 0, 1, 2)    
        controlsLayout.addWidget(self.showStagingRoomBtn,   3, 0, 1, 1)    
        controlsLayout.addWidget(self.showRegisterBtn,      3, 1, 1, 1)    
        controlsLayout.addWidget(self.showDatabaseBtn,      4, 0, 1, 1)    
        controlsLayout.addWidget(self.updateSchemaBtn,      4, 1, 1, 1)    
        controlsLayout.addWidget(self.processModeGroupBox,  5, 0, 1, 1)    
        controlsLayout.addWidget(self.rollbackBtn,          5, 1, 1, 1)    

        layout.addLayout(controlsLayout)    
        
        
        
        #layout.setColumnStretch( 0, 1 ) # Give column 0 no stretch ability
        #layout.setColumnStretch( 1, 0 ) # Give column 1 stretch ability of ratio 1        
        
        self.setLayout(layout)


        self.drawSchema()
                
                
    def rollback(self):
        selectedBlock   = self.canevas.getSelectedBlock().block  
        
        # We reverse the list such that it starts by the leafs and go down to 
        # the root such that we rollback in inverse order of the execution.
        blockToRollback = list(reversed(selectedBlock.getNextBlocks(recursive=True))) 
        blockToRollback.append(selectedBlock.ID)
        

        reply = QtGui.QMessageBox.question(self, 'Rolling back',
            "This operation is about to cancel every operation performed by the"\
            "selected block and the " + str(len(blockToRollback)-1) + \
            " blocks following it. Are you sure that you want to proceed?", 
                                    QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
                                    
        if reply == QtGui.QMessageBox.Yes:
            if self.register is None:
                self.register = RegisterWrapper(self.registerName)            

            if not self.register.isValid():  
                self.startRegister()             
                if not self.register.isValid():                  
                    QtGui.QMessageBox.critical(self, "Error connecting to the register", 
                         "The register associated to the selected schema is not online."\
                         " Tentative to start it has failed. The rollback is  "\
                         "cannot be performed in this context.")
                    return 
                    
            for ID in blockToRollback:
                self.register.getBlock(ID).rollback()

        self.drawSchema()  
      
        
                
    def changeProcessMode(self):
        if self.horizontalBtn.isChecked():
            warehouse.proxy(self.stagingRoomName).changeMode("horizontal")   
        elif self.horizontalBtn.isChecked():      
            warehouse.proxy(self.stagingRoomName).changeMode("vertical")              
        
              
    def showDatabase(self):
        dlg = DatabaseDialog(self, self.databaseName)
        dlg.show()
                        
                
                
    def showStagingRoom(self):
        dlg = StagingRoomDialog(self, self.stagingRoomName, self.HMAC_KEY)
        dlg.show()
                                
                
    def showRegister(self):
        dlg = RegisterDialog(self, self.registerName, self.HMAC_KEY)
        dlg.show()
                        
                
    @property
    def HMAC_KEY(self):
        return self._HMAC_KEY
        
    @HMAC_KEY.setter
    def HMAC_KEY(self, HMAC_KEY):
        self._HMAC_KEY = HMAC_KEY
        Pyro4.config.HMAC_KEY = HMAC_KEY          
        sys.excepthook = Pyro4.util.excepthook    
                      
    """              
    def blockStatusChanged(self, blockID, status):
        
        if self.canonicalBtn.isChecked() :
            # If the canonical schema is drawn, block status has no meaning thus 
            # there is nothing to update.
            return
        

        if not blockID in self.drawnBlockItems :
            # If the block has not been added to the drawn schema so far the whole 
            #schema must be redrawn.            
            self.drawSchema()
        
        else :
            # Else, only the specific block that has changed status has to be
            # updated.
            self.drawnBlockItems[blockID].blockStatus = status
    """     
            
    
    

    def drawSchema(self, schema=None, registerName=None):
        if not schema is None:
            self.schema         = schema
        if not registerName is None:
            self.registerName   = registerName        
            
        if self.expandedBtn.isChecked():
            self.register = RegisterWrapper(self.registerName)
            if not self.register.isValid():  
                self.startRegister()             
                if not self.register.isValid():                  
                    QtGui.QMessageBox.critical(self, "Error connecting to the register", 
                         "The register associated to the selected schema is not online."\
                         " Tentative to start it has failed. The expanded view "\
                         "cannot be displayed in this context. "\
                         "Falling back to the canonical view.")
                    self.canonicalBtn.setChecked(True)
                    
        self.drawnBlockItems = {}
        self.canevas.clear()
        self.grid = []
        
        if not self.schema is None:  
            
            if self.canonicalBtn.isChecked():
                # For the canonical schema, we use the blocks in self.schema
                startBlock = self.schema.getBlock(self.schema.startBlock)  
                
            #elif self.mixedBtn.isChecked():
            #    self.drawMixed()  
                
            elif self.expandedBtn.isChecked():
                # We get the schema start block...
                schemaStartBlock = self.schema.getBlock(self.schema.startBlock)  
                # ...and look up for the registered version (which might be updated
                # compared to the schema version).    
                startBlock = self.register.getBlock(schemaStartBlock.ID)
            
            levelZero = []
            self.grid.append(levelZero)            
            levelZero.append(self.drawNextBlocks(startBlock, None, 0))   
            
            self.positionBlocks()
       

    def startRegister(self):
        self.startRegisterRequest.emit()
        #sleep(2)
        self.register = RegisterWrapper(self.registerName)   
 
 
    def drawNextBlocks(self, currentBlock, previousBlockItem, level):
    
        if currentBlock.ID in self.drawnBlockItems:
            """
            If the block has already been added to the displayed schema (this can happen
            when the output of two blocks goes to the input of a same block), just 
            reuse the existing block.
            """            
            currentBlockItem = self.drawnBlockItems[currentBlock.ID]     
            self.setBlockItemStatus(currentBlock, currentBlockItem)

        else:
            """
            If the block has never been added to the displayed schema, create this
            block adding it the appropriate ports and add it to the displayed schema.
            """    
            
            currentBlockItem = ProcessBlock(block = currentBlock)  
            for name, value in currentBlock.inputs.iteritems():
                currentBlockItem.addInputPort(name)          
                
            for blockOutput in currentBlock.outputs:
                currentBlockItem.addOutputPort(blockOutput)
      
            currentBlockItem.guessBestGeometry()
    
            currentBlockItem.blockFocusInEvent.connect(self.blockFocusInEvent)
            currentBlockItem.blockFocusOutEvent.connect(self.blockFocusOutEvent)                
                
            self.setBlockItemStatus(currentBlock, currentBlockItem)              
            self.canevas.addBlock(currentBlockItem)    
        

        """
        Add the appropriate arrows between the added block and its previous block.
        """        
        for name, value in currentBlock.inputs.iteritems():
            if isinstance(value, fromPrevious):
                link = ArrowLink()
                if previousBlockItem.block.outputs[value.outputKey] == "parallel":
                    link.type = (1, np.inf)
                elif  previousBlockItem.block.outputs[value.outputKey] == "atomic":
                    link.type = (1, 1)
                else:
                    raise ValueError("No rules to intepret output port of types " \
                        + str(previousBlockItem.block.outputs[value.outputKey]))
                
                previousBlockItem.outputPorts[value.outputKey].addLink(link)
                currentBlockItem.inputPorts[name].addLink(link)
                self.canevas.scene.addItem(link)
                



        """
        If the block has just been added to the schema, process its following 
        blocks.
        """
        if not currentBlock.ID in self.drawnBlockItems:
            self.drawnBlockItems[currentBlock.ID] = currentBlockItem            
            
            if len(currentBlock.nextBlockIDs):
                if len(self.grid) <= level + 1:
                    levelN = []
                    self.grid.append(levelN)    
                else:
                    levelN = self.grid[level+1]
                    
                for nextBlockID in currentBlock.nextBlockIDs:
                    if self.canonicalBtn.isChecked():
                        # For the canonical schema, we use the blocks in self.schema
                        nextBlock = self.schema.getBlock(ID = nextBlockID)
                        
                    #elif self.mixedBtn.isChecked():
                    #    self.drawMixed()  
                        
                    elif self.expandedBtn.isChecked():
                        # For the expanded schema, we use the blocks registered.                
                        nextBlock = self.register.getBlock(nextBlockID)

                    
                    levelN.append(self.drawNextBlocks(nextBlock, currentBlockItem, level+1))   
                    #if self.expandedBtn.isChecked():
                    #    for siblinBlock in nextBlock.siblingBlocks:
                    #        levelN.append(self.drawNextBlocks(siblinBlock, 
                    #                                          currentBlockItem, level+1))                                                                   
                                                              
                                       
                
        return currentBlockItem
       
    
    
    
    def setBlockItemStatus(self, block, blockItem):
        if self.expandedBtn.isChecked():
            # For the expanded schema, we use the blocks registered.                
            if self.register.hasExecuted(block)  :
                blockItem.blockStatus = "executed"
            elif self.register.isInError(block) :  
                blockItem.blockStatus = "inError"
            elif self.register.hasStarted(block) :  
                blockItem.blockStatus = "started"

    def positionBlocks(self):
        """
        Autoposition blocks according to a rectangular grid.
        """ 
        rowHeights      = np.zeros(max([len(col) for col in self.grid]))
        columnWidths    = np.zeros(len(self.grid))

        for noCol, col in enumerate(self.grid):
            columnWidths[noCol] = max([block.width for block in col])
            for noRow, block in enumerate(col):
                rowHeights[noRow] = max(rowHeights[noRow], block.height)
            
        
        rowPos = np.concatenate(([0], np.cumsum(rowHeights   + self.betweenblockspace)))[:-1] 
        colPos = np.concatenate(([0], np.cumsum(columnWidths + self.betweenblockspace)))[:-1] 

        nbMaxRox = max([len(col) for col in self.grid])

        for noCol, col in enumerate(self.grid):
            columnWidths[noCol] = max([block.width for block in col])
            for noRow, block in enumerate(col):
                if len(col) > 1 :
                    if len(col)-1:
                        iRow = (noRow)/float(len(col)-1) *(nbMaxRox -1)
                    else:
                        iRow = 0.0
                else:
                    iRow = 0.5*(nbMaxRox -1)
                    
                block.x = colPos[noCol]
                
                y1 = rowPos[np.floor(iRow)] 
                y2 = rowPos[np.ceil(iRow)]
                block.y = y1 + (y2-y1)*(iRow - np.floor(iRow))
    

    
    def blockFocusInEvent(self, block):
        self.blockInFocus = block
        self.propertyWidget.setTableData({"input":block.inputs, "output":block.outputs})        
        if not self.comHubName is None:
            #comHub.addBlockListener(block.ID, self)
            self.console.clear()
            message = warehouse.proxy(self.comHubName).readBlockMessages(block.ID)
            self.console.addMessage(message)        
    
    
    
    def blockMessageEmitted(self, message) :
        self.console.addMessage(message)
 
        
        
    def blockFocusOutEvent(self):
        self.propertyWidget.setTableData({})
        #if not self.comHubName is None:
            #with Pyro4.Proxy("PYRONAME:" + self.comHubName) as comHub:  
                #comHub.removeBlockListener(self.blockInFocus.ID, self)  
        self.blockInFocus = None
  

