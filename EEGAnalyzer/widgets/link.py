# -*- coding: utf-8 -*-
"""
Created on Fri Jul 12 00:36:50 2013

@author: Christian
"""



from PySide.QtCore import *
from PySide.QtGui import *

from schemaobject import SchemaObject

class Link(SchemaObject):

    def __init__(self, parent) :
        
        super(Link, self).__init__(parent)
        
        self.line = QLineF()        
        
        # Type of relationship. Should be either None or (N, M) where N and M
        # are integers indicating the number of item on left and right-hand side
        # of the relation.
        self.type = None

        #if parent :
            #self.setAutoFillBackground(True)
            #self.setGeometry(self.parent.rect())
            #self.setFocusPolicy(Qt.StrongFocus)
            #self.setBackgroundRole(QPalette.Window)
    
            # set black background
            #pal = self.palette()
            #pal.setColor(QPalette.Window, Qt.black);
            #self.setPalette(pal)

    """
    def paintEvent(self, event):
    
        pal = self.palette()
        
        # set black background
        if self.hasFocus():
            pal.setColor(QPalette.Window, Qt.red)
        else :
            pal.setColor(QPalette.Window, Qt.black)
        self.setPalette(pal)
        super(Link, self).paintEvent(event)
    """
    

    @property
    def p1(self):
        return self.line.p1()
        
    @p1.setter
    def p1(self, p1):
        self.prepareGeometryChange()
        oldP1    = self.line.p1()
        self.line.setP1(p1)
        self.redefine(oldP1, self.p2)


    @property
    def p2(self):
        return self.line.p2()
        
    @p2.setter
    def p2(self, p2):
        self.prepareGeometryChange()
        oldP2    = self.line.p2()
        self.line.setP2(p2)
        self.redefine(self.p1, oldP2)
    
    


    @property
    def x2(self):
        return self.p2.x()
    @property
    def x1(self):
        return self.p1.x()

    @property
    def y2(self):
        return self.p2.y()
    @property
    def y1(self):
        return self.p1.y()

    

    def boundingRect(self):
        return QRect(QPoint(min(self.x1, self.x2), min(self.y1, self.y2)),
                     QPoint(max(self.x1, self.x2), max(self.y1, self.y2)))

    def paint(self, painter, option, widget):
        painter.drawLine(QLine(self.x1, self.y1, self.x2, self.y2))    
    
    

    def mousePressEvent(self, event): 
        self.update()


    def setLine(self, x1, y1, x2, y2):
        self.p1 = QPoint(x1, y1)
        self.p2 = QPoint(x2, y2)


    # For walking though the schema. Arrows are not aware of the object they are connected to.
    def getNbPreviousObject()   : return 0
    def getNbNextObject()       : return 0
    def previousObject()        : return None
    def nextObject()            : return None




class ArrowLink(Link):
    
   
    # Emitted when the focus has changed from one object to another in the canevas.
    # This allows, for exemple, to update the object properties window. The newly
    # selected object is passed as parameter.
    updateEvent = Signal(QRegion)    
    
    def __init__(self, parent=None) :
        super(ArrowLink, self).__init__(parent)
    
        #pal = self.palette()
        ##set black background
        #pal.setColor(QPalette.Window, Qt.black)
        #self.setAutoFillBackground(True)
        #self.setBackgroundRole(QPalette.Window)
        #self.setPalette(pal)
        
        self.setLine(0, 0, 0, 0)


    def redefine(self, oldP1, oldP2):
        self.update()
        pass
        """
        if self.parentWidget() :
            
            #if not self.p1 is None and not self.p2 is None:
            mask = QBitmap(self.parentWidget().size())
            mask.clear()
            
            painter = QPainter()
            painter.begin(mask)
            painter.setPen(Qt.color1)
            painter.drawLine(self.x1, self.y1, self.x2, self.y2)
            painter.end()
            
            self.setMask(mask)
            self.show()

            oldRegion = QRegion(Polygon([oldP1, oldP2]))
            newRegion = QRegion(Polygon([self.p1, self.p2]))
            self.updateEvent.emit(newRegion.united(oldRegion))
        """





    def formatObjectCode(self): pass




