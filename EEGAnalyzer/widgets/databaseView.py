# -*- coding: utf-8 -*-
"""
Created on Wed Nov 13 19:39:48 2013

@author: oreichri
"""

from PySide import QtGui, QtCore
from PyBlockWork.registerWrapper import RegisterWrapper
import pandas as pd
import sqlalchemy as sa
from spyndle.io import Session, Base, EventClass, DatabaseMng #, DataModelMng



class DatabaseDialog(QtGui.QDialog):
    """
     Main dialog for viewing the database information.
    """

    def __init__(self, parent, databaseName):
        super(DatabaseDialog, self).__init__(parent)
        self.setWindowTitle("Database content");
        self.databaseView = TableListView(self, databaseName)
        self.databaseView.close.connect(self.close)

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.databaseView)
        self.setLayout(layout)






class TableListView(QtGui.QWidget):
    """
     Widget displaying the list of available tables in a database and opening
     a dialog to view a table content upon double click on one of the table
     name in the list.
    """
    close = QtCore.Signal()  
    
    def __init__(self, parent, databaseName) :
        super(TableListView, self).__init__(parent)

        # Defaultspace between blocks when using automatic placement
        if databaseName is None:
            text, ok = QtGui.QInputDialog.getText(self, 'There is no active database. Which database do you want to view?', 
            'Database path:')
        
            if ok:
                self.databaseName = str(text)  
            else:
                QtGui.QMessageBox.critical(self, "Database error", 
                                     "No database to show.")   
                return
        else:
            self.databaseName = databaseName

        
    
        
        try:
            self.dbEngine = sa.create_engine(self.databaseName)
            Session.configure(bind=self.dbEngine)   
            self.session = Session()

        except sa.exc.ArgumentError, error:
            errMsg = "Error connecting to the specified database URL. "\
                     "The format of the database path is invalid."     \
                     "\n\nSQLAlchemy error message: " + str(error)
            raise sa.exc.ArgumentError(errMsg)

    
        

        self.listQueue          = QtGui.QListWidget(self)
        #self.refreshBtn         = QtGui.QPushButton("Refresh", self)
        #self.refreshBtn.clicked.connect(self.refresh)

        layout         = QtGui.QVBoxLayout()
        layout.addWidget(QtGui.QLabel("Available tables"))
        layout.addWidget(self.listQueue)
        #layout.addWidget(self.refreshBtn)
             
        self.listQueue.itemDoubleClicked.connect(self.openTable)             
             
        self.setLayout(layout)
        #self.refresh()

        self.tableList =  Base.metadata.tables.keys()
        for table in self.tableList:
            self.listQueue.addItem(table)
        
                
        
    def openTable(self, qListWidgetItem):
        tableName = str(qListWidgetItem.text())
        
        dlg = TableViewDialog(self, tableName, self.databaseName )
        dlg.show()










class TableViewDialog(QtGui.QDialog):
    """
     Dialog used to diplay the content of a database table.
    """
    def __init__(self, parent, tableName, databaseName ):
        super(TableViewDialog, self).__init__(parent)
        self.setWindowTitle("Table " + tableName + " content");
        self.tableView = TableView(self, tableName, databaseName)
        self.tableView.close.connect(self.close)

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.tableView)
        self.setLayout(layout)






class TableView(QtGui.QWidget):
    """
     Widget displayin the content of a datable table.
    """
    close = QtCore.Signal()  
    
    def __init__(self, parent, tableName, databaseName ) :
        super(TableView, self).__init__(parent)

        self.tableName      = tableName  
        self.databaseName   = databaseName  
  
        #self.listWidget         = QtGui.QListWidget(self)
        self.refreshBtn         = QtGui.QPushButton("Refresh", self)
        self.refreshBtn.clicked.connect(self.refresh)

        self.createTable() 

        layout         = QtGui.QVBoxLayout()
        layout.addWidget(QtGui.QLabel("Registered blocks"))
        #layout.addWidget(self.listWidget)
        layout.addWidget(self.tableView) 
        layout.addWidget(self.refreshBtn)
             
        self.setLayout(layout)
        self.refresh()



    def refresh(self):
        
        self.tableModel.setTableData(self.tableName, self.databaseName)

                
        
    def createTable(self):
        # create the view
        self.tableView = QtGui.QTableView()

        # set the table model
        self.tableModel = TableModel(self) 
        self.tableView.setModel(self.tableModel)

        # set the minimum size
        self.tableView.setMinimumSize(400, 300)

        # hide grid
        self.tableView.setShowGrid(False)

        # set the font
        font = QtGui.QFont("Courier New", 8)
        self.tableView.setFont(font)

        # hide vertical header
        vh = self.tableView.verticalHeader()
        vh.setVisible(False)

        # set horizontal header properties
        hh = self.tableView.horizontalHeader()
        hh.setStretchLastSection(True)

        # set column width to fit contents
        self.tableView.resizeColumnsToContents()

        # set row height
        for row in xrange(self.tableModel.rowCount()):
            self.tableView.setRowHeight(row, 18)

        # enable sorting
        self.tableView.setSortingEnabled(True)


        







class TableModel(QtCore.QAbstractTableModel): 
    def __init__(self, parent=None, *args): 
        """ datain: a list of lists
            headerdata: a list of strings
        """
        QtCore.QAbstractTableModel.__init__(self, parent, *args) 
        
        self.modelData   = pd.DataFrame()     
        self.showCol     = {}
        self.visibleCols = []
        
    def rowCount(self, parent=None): 
        return self.modelData.shape[0]
 
    def columnCount(self, parent=None): 
        return sum(self.showCol.values())
 
 
    def data(self, index, role):
        if self.columnCount() == 0 or not index.isValid():
            return None       
        
        if role != QtCore.Qt.DisplayRole: 
            return None
            
        return self.modelData.ix[index.row(), self.visibleCols[index.column()]] 



    def headerData(self, col, orientation, role):
        colName = self.visibleCols[col]
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole and self.showCol[colName]:
            return colName
        return None


    def sort(self, Ncol, order):
        """Sort table by given column number.
        """
        if self.columnCount() == 0:
            return

        #self.layoutAboutToBeChanged.emit()
        
        if order == QtCore.Qt.DescendingOrder:
            ascending  = False           
        else:
            ascending  = True            
            
        self.modelData.sort(self.visibleCols[Ncol], ascending=ascending, inplace=True)
        self.modelData.reset_index(drop=True, inplace=True)            
            
        topLeft     = self.createIndex(0, 0) 
        bottomRight = self.createIndex(self.rowCount()-1, self.columnCount()-1)
        self.dataChanged.emit(topLeft, bottomRight)

        #self.layoutChanged.emit()
        


    def setTableData(self, tableName, databaseName):

        table = Base.metadata.tables[tableName]
        for colName in table.columns:
            print str(colName).split(".")[1]

        dbm = DatabaseMng(databaseName)
        print dbm.session.query(Base.metadata.tables[tableName]).all()


        """

        IDs             = []
        hasExecuteds    = []
        hasStarteds     = []
        blockStrings    = []
        inputs          = []
        outputs         = []
        inputs          = []
        names           = []
        nextBlockIDs    = []
        previousBlockIDs= []
        
        
        for ID, string in register.iteritems():
            registerItemDict = eval(string) 
            IDs.append(ID)
            hasExecuteds.append(str(registerItemDict["hasExecuted"]))
            hasStarteds.append(str(registerItemDict["hasStarted"]))
            block = eval(registerItemDict["blockString"])
            inputs.append(str(block["inputs"]))
            outputs.append(str(block["outputs"]))
            names.append(str(block["name"]))
            nextBlockIDs.append(str(block["nextBlockIDs"]))
            previousBlockIDs.append(str(block["previousBlockIDs"]))
            
            del block["inputs"]
            del block["outputs"]
            del block["name"]
            del block["nextBlockIDs"]
            del block["previousBlockIDs"]
            blockStrings.append(str(block))
            
            
        
        self.layoutAboutToBeChanged.emit()        
        self.modelData = pd.DataFrame({"ID"             : IDs, 
                                      "name"            : names,
                                      "next blocks"     : nextBlockIDs,
                                      "previous blocks" : previousBlockIDs,
                                      "executed"        : hasExecuteds,
                                      "started"         : hasStarteds,
                                      "inputs"          : inputs,
                                      "outputs"         : outputs,
                                      "blockString"     : blockStrings})   
                      
        self.showCol = {"ID"                : True, 
                        "name"              : True, 
                        "next blocks"       : True, 
                        "previous blocks"   : True, 
                        "executed"          : True,
                        "started"           : True,
                        "inputs"            : True,
                        "outputs"           : True,
                        "blockString"       : True}   
 
        self.visibleCols = [name for name in self.modelData.columns.tolist() if self.showCol[name]]                            

        self.layoutChanged.emit()
        """

   