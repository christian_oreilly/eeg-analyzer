# -*- coding: utf-8 -*-
"""
Created on Fri Nov 08 15:34:02 2013

@author: oreichri
"""

from PySide import QtGui

class ConsoleWidget(QtGui.QTextEdit):
    
    def addMessage(self, message):
        self.setText(self.toPlainText() + message + "\n")
        self.moveCursor(QtGui.QTextCursor.End);
        
    def clear(self):
        self.setText("")
        
        