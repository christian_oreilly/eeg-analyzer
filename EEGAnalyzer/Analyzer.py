# -*- coding: utf-8 -*-

#!/usr/bin/env python

import sys

from PySide import QtCore, QtGui
import sqlalchemy
import os
import Pyro4
import time
import socket
from socket import error as socket_error

from spyndle.io import DatabaseMng

from PyBlockWork import Schema, PyroServer
from PyBlockWork import CommunicationHub, StagingRoom, Register

from EEGAnalyzer import persistReg, SchemaDict, SchemaPersistObject

from widgets.processingNodeWidget import ProcessingNodeWidget
from widgets.comConsumerWidget import ComConsumerWidget
from widgets.schemaView import SchemaView


#import warnings
#warnings.simplefilter('error', UserWarning)


dbMng = DatabaseMng()

"""
class ServerWidget(QtGui.QWidget):
    
    def __init__(self, parent = None):
        super(ServerWidget, self).__init__(parent)            

        self.serverPath   = QtGui.QLineEdit("localhost")   
        self.connectBtn   = QtGui.QPushButton("Connect")   
        self.disconnecBtn = QtGui.QPushButton("Disconnect")   
        self.serverStatus = QtGui.QLabel("Unconnected. Working on localhost.") 

        self.layout().addWidget(QtGui.QLabel("Server:"))
        self.layout().addWidget(self.serverPath)    
        self.layout().addWidget(self.setServerBtn)    
        self.layout().addWidget(self.serverStatus)    
"""        
        
        

class DatabaseWidget(QtGui.QWidget):
    
    def __init__(self, parent = None):
        super(DatabaseWidget, self).__init__(parent)        
      
        self.databasePath   = QtGui.QLineEdit()     
        
        self.completer = QtGui.QCompleter(persistReg.getList("DatabaseWidget.databasePath"), self)
        self.completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.completer.setCompletionMode(QtGui.QCompleter.UnfilteredPopupCompletion)
        self.databasePath.setCompleter(self.completer)    

        self.databasePath.returnPressed.connect(self.databasePathReturned)
        self.databasePath.installEventFilter(self)

        self.connectBtn     = QtGui.QPushButton("Connect")
        self.disconnectBtn  = QtGui.QPushButton("Disconnect")
        self.viewBtn        = QtGui.QPushButton("View")
        self.newBtn         = QtGui.QPushButton("New")
        
        self.connectBtn.clicked.connect(self.connectDatabase)
        self.disconnectBtn.clicked.connect(self.disconnectDatabase)
        self.viewBtn.clicked.connect(self.viewDatabase)
        self.newBtn.clicked.connect(self.createDatabase)
        
        self.disconnectBtn.setEnabled(False)
        self.viewBtn.setEnabled(False)
        
        self.setLayout(QtGui.QHBoxLayout())
      
        self.layout().addWidget(QtGui.QLabel("Database:"))
        self.layout().addWidget(self.databasePath)
        self.layout().addWidget(self.connectBtn)
        self.layout().addWidget(self.disconnectBtn)
        self.layout().addWidget(self.viewBtn)
        self.layout().addWidget(self.newBtn)




    def eventFilter(self, object, event):    
        
        if object == self.databasePath:
            if event.type() == QtCore.QEvent.Type.MouseButtonRelease :
                if event.button()== QtCore.Qt.LeftButton :
                    self.completer.complete()
                    return True
        return False



    def databasePathReturned(self):
        
        if self.databasePath.text() == "":
            self.completer.complete()
        else:
            self.connectDatabase()

    def connectDatabase(self) :
        
        try:
            dbMng.connectDatabase(self.databasePath.text())

            if dbMng.isConnected():
                self.connectBtn.setEnabled(False)
                self.disconnectBtn.setEnabled(True)
                self.databasePath.setEnabled(False)
                self.viewBtn.setEnabled(True)
            
            persistReg.addToList("DatabaseWidget.databasePath", self.databasePath.text())            
            
        except sqlalchemy.exc.ArgumentError, e:
             QtGui.QMessageBox.critical(self, "Error connecting to the specified database URL", 
                                 "The format of the database path is invalid."\
                                 "\n\nSQLAlchemy error message: " + str(e))

        
    def disconnectDatabase(self):
        dbMng.disconnectDatabase()
        
        if not dbMng.isConnected():
            self.connectBtn.setEnabled(True)
            self.disconnectBtn.setEnabled(False)
            self.databasePath.setEnabled(True)
            self.viewBtn.setEnabled(False)        
        
    def viewDatabase(self) :
        pass
        
        
    def createDatabase(self):
        pass
        
        






class SchemaPropertyWidget(QtGui.QWidget):
    
    def __init__(self, parent = None):
        super(SchemaPropertyWidget, self).__init__(parent)
        
        self.setLayout(QtGui.QGridLayout())

        self.startBtn       = QtGui.QPushButton("Start")
        self.stopBtn        = QtGui.QPushButton("Stop")
        self.propertiesBtn  = QtGui.QPushButton("Edit schema properties")
        self.viewBtn        = QtGui.QPushButton("View schema")
        self.addBtn         = QtGui.QPushButton("Add schema")
        self.removeBtn      = QtGui.QPushButton("Remove selected schema")
        
        self.filePath               = QtGui.QLineEdit()
        self.requestedNbProcesses   = QtGui.QLineEdit() 
        self.registerName           = QtGui.QLineEdit() 
        self.registerPath           = QtGui.QLineEdit()         
        self.isRunning              = QtGui.QLineEdit()   
        
        
        self.filePath.textChanged.connect(self.filePathChanged)
        self.requestedNbProcesses.textChanged.connect(self.requestedNbProcessesChanged) 
        self.registerName.textChanged.connect(self.registerNameChanged) 
        self.registerPath.textChanged.connect(self.registerPathChanged)          

        self.setEnabled(False)         
        self.isRunning.setEnabled(False)   
        
        
        self.layout().addWidget(QtGui.QLabel("File path:"),                 0, 0, 1, 1)
        self.layout().addWidget(self.filePath ,                             0, 1, 1, 1)
        self.layout().addWidget(QtGui.QLabel("Requested # of processes:"),  1, 0, 1, 1)
        self.layout().addWidget(self.requestedNbProcesses,                  1, 1, 1, 1)
        self.layout().addWidget(QtGui.QLabel("register name:"),             2, 0, 1, 1)
        self.layout().addWidget(self.registerName,                          2, 1, 1, 1)
        self.layout().addWidget(QtGui.QLabel("Register path:"),             3, 0, 1, 1)
        self.layout().addWidget(self.registerPath,                          3, 1, 1, 1)
        self.layout().addWidget(QtGui.QLabel("Is running:"),                4, 0, 1, 1)
        self.layout().addWidget(self.isRunning,                             4, 1, 1, 1)
        
        self.SchemaPersistObject = None










    def setEnabled(self, enabled):
        self.filePath.setEnabled(enabled)  
        self.requestedNbProcesses.setEnabled(enabled)  
        self.registerName.setEnabled(enabled)  
        self.registerPath.setEnabled(enabled)         



    def setSchemaPersistObject(self, schemaPersistObject):
        if schemaPersistObject is None:
            self.schemaPersistObject = None
            self.filePath.setText("")
            self.requestedNbProcesses.setText("")
            self.registerName.setText("")
            self.registerPath.setText("")
            self.isRunning.setText("")
            self.setEnabled(False)
        else:
            self.schemaPersistObject = schemaPersistObject
            self.filePath.setText(schemaPersistObject.filePath)
            self.requestedNbProcesses.setText(str(schemaPersistObject.requestedNbProcesses))
            self.registerName.setText(schemaPersistObject.registerName)
            self.registerPath.setText(schemaPersistObject.registerPath)
            self.isRunning.setText(str(schemaPersistObject.isRunning))
            self.setEnabled(True)
   
   
    def filePathChanged(self):
        if not self.schemaPersistObject is None:
            self.schemaPersistObject.filePath = self.filePath.text()
       
    def requestedNbProcessesChanged(self):
        if not self.schemaPersistObject is None:
            self.schemaPersistObject.requestedNbProcesses = int(self.requestedNbProcesses.text())
        
        
    def registerNameChanged(self):
        if not self.schemaPersistObject is None:
            self.schemaPersistObject.registerName = self.registerName.text()
        
        
    def registerPathChanged(self):
        if not self.schemaPersistObject is None:
            self.schemaPersistObject.registerPath = self.registerPath.text()
          
   
   
   

class SchemaWidget(QtGui.QWidget):
    
    def __init__(self, parent = None):
        super(SchemaWidget, self).__init__(parent)
        self.mainWindow = parent      
        
        #QtGui.QtTreePropertyBrowser()        
        
        self.setLayout(QtGui.QGridLayout())

        self.schemaListWidget     = QtGui.QListWidget()     
        #self.schemaListWidget.setSelectionMode(QtGui.QAbstractItemView.MultiSelection)

        self.startBtn       = QtGui.QPushButton("Start")
        self.stopBtn        = QtGui.QPushButton("Stop")
        self.propertiesBtn  = QtGui.QPushButton("Edit schema properties")
        self.viewBtn        = QtGui.QPushButton("View schema")
        self.addBtn         = QtGui.QPushButton("Add schema")
        self.removeBtn      = QtGui.QPushButton("Remove selected schema")
        
        self.propertyWidget = SchemaPropertyWidget(self)
        
        self.removeBtn.setEnabled(False)
        self.stopBtn.setEnabled(False)
        self.startBtn.setEnabled(False)
        self.propertiesBtn.setEnabled(False)   
        self.viewBtn.setEnabled(False)             


        self.layout().addWidget(QtGui.QLabel("BlockWork schema:"),  0, 0, 6, 1)
        self.layout().addWidget(self.schemaListWidget,              0, 1, 6, 1)
        self.layout().addWidget(self.propertyWidget,                0, 2, 6, 1)
        self.layout().addWidget(self.startBtn ,                     0, 3, 1, 1)
        self.layout().addWidget(self.stopBtn ,                      1, 3, 1, 1)
        self.layout().addWidget(self.propertiesBtn ,                2, 3, 1, 1)
        self.layout().addWidget(self.viewBtn ,                      3, 3, 1, 1)
        self.layout().addWidget(self.addBtn ,                       4, 3, 1, 1)
        self.layout().addWidget(self.removeBtn,                     5, 3, 1, 1)
        self.schemaListWidget.itemSelectionChanged.connect(self.schemaSelectionChanged)
        
        self.startBtn.clicked.connect(self.startSchema)
        self.stopBtn.clicked.connect(self.stopSchema)
        self.propertiesBtn.clicked.connect(self.editSchemaProperties)
        self.viewBtn.clicked.connect(self.viewSchema)
        self.addBtn.clicked.connect(self.addchema)
        self.removeBtn.clicked.connect(self.removechema)
        
        self._schemaDict = persistReg.getObject("SchemaWidget.schemaDict")
        if self.schemaDict is None:
            self.schemaDict = SchemaDict("SchemaWidget.schemaDict")
        else:
            for schemaKey in self.schemaDict:
                self.schemaDict[schemaKey].schemaDictObj = self.schemaDict            
            
        self.updateSchemaListWidget()


     

    @property
    def schemaDict(self):
        return self._schemaDict
        
    @schemaDict.setter
    def schemaDict(self, value):
        self._schemaDict = value    
        persistReg.setObject("SchemaWidget.schemaDict", value)
        

    def schemaSelectionChanged(self):
        if (not self.schemaListWidget.currentItem() is None and 
                                    self.schemaListWidget.currentRow() >= 0) :
                                        
            schemaPersistObject = self.schemaDict[self.schemaListWidget.currentItem().text()]
            self.removeBtn.setEnabled(True)
            self.propertiesBtn.setEnabled(True)   
            self.viewBtn.setEnabled(True)               
            
            self.startBtn.setEnabled(not schemaPersistObject.isRunning)   
            self.stopBtn.setEnabled(schemaPersistObject.isRunning)           
            
            self.propertyWidget.setSchemaPersistObject(schemaPersistObject)



            schemaName = self.schemaListWidget.currentItem().text()
            try :
                schema = Schema.load(schemaName)
            except IOError, e:
                 QtGui.QMessageBox.critical(self, "Error opening file.", 
                                     "Error: " + str(e))        

            self.mainWindow.schemaView.drawSchema(schema, self.schemaDict[schemaName].registerName)
            
            #self.mainWindow.schemaView.registerName  = self.schemaDict[schemaName].registerName



        else:
            self.removeBtn.setEnabled(False)
            self.propertiesBtn.setEnabled(False)   
            self.viewBtn.setEnabled(False)               
            self.startBtn.setEnabled(False)   
            self.stopBtn.setEnabled(False)                    

            self.propertyWidget.setSchemaPersistObject(None)

            #self.mainWindow.schemaView = SchemaView(self, None, None) 
            self.mainWindow.schemaView.registerName  = None


    def startSchema(self):

        schemaName = self.schemaListWidget.currentItem().text()
        try :
            schema = Schema.load(schemaName)
        except IOError, e:
             QtGui.QMessageBox.critical(self, "Error opening file.", 
                                 "Error: " + str(e))
                        
            
        self.mainWindow.checkNameServer()

        # We just start the register here because it is the only object that should
        # be different from schema to schema. The stagingRoom and the communicationHub
        # are started at same time as the name server.
        registerName = self.schemaDict[schemaName].registerName
        register     = Register(self.schemaDict[schemaName].registerPath)
        self.mainWindow.pyroNSWidget.registerObject(register, registerName)

        HMAC_KEY = str(self.mainWindow.pyroNSWidget.hmacKeyWidget.text()) 
        schema.start("stagingRoom", registerName, HMAC_KEY)

    
    def stopSchema(self):
        pass
    
    def editSchemaProperties(self):
        pass
    
    def viewSchema(self):
        pass
        """
        schemaName = self.schemaListWidget.currentItem().text()
        try :
            schema = Schema.load(schemaName)
        except IOError, e:
             QtGui.QMessageBox.critical(self, "Error opening file.", 
                                 "Error: " + str(e))        
        
        self.mainWindow.schemaView = SchemaView(self, schema, self.schemaDict[schemaName].registerName)  
        schemaName = "schema " + self.schemaDict[schemaName].registerName
        self.mainWindow.docks[schemaName] = QtGui.QDockWidget(schemaName, self.mainWindow)
        self.mainWindow.docks[schemaName].setWidget(self.mainWindow.schemaView)          
        self.mainWindow.addDockWidget(QtCore.Qt.BottomDockWidgetArea, self.mainWindow.docks[schemaName])
        """
        
        

    
    
    
    
    
    def addchema(self):
        path = persistReg.getObject("loadSavePath") 
        if path is None:
            path = ""

        fname, _ = QtGui.QFileDialog.getOpenFileName(self, 'Select the schema file to add', path)
        if os.path.isfile(fname):
            if fname in self.schemaDict:
                 QtGui.QMessageBox.critical(self, "Error adding schema", 
                                     "This schema is already in your schema list.")                
            else:
                persistReg.setObject("loadSavePath", os.path.dirname(fname)) 
                self.schemaDict[fname] = SchemaPersistObject(fname, "SchemaWidget.schemaDict", self.schemaDict)
                self.schemaListWidget.addItem(fname)       



    def removechema(self):
        del self.schemaDict[self.schemaListWidget.currentItem().text()]   
        self.schemaListWidget.takeItem(self.schemaListWidget.currentRow())

                
                

    def updateSchemaListWidget(self):
        self.schemaListWidget.clear()
        for key in self.schemaDict:
           #if self.schemaDict[key].isRunning :
            #    key += " - Running..."
            self.schemaListWidget.addItem(key)            




class PyroNSWidget(QtGui.QWidget):

    def __init__(self, parent = None):
        super(PyroNSWidget, self).__init__(parent)
        self.mainWindow = parent
        
        self.setLayout(QtGui.QHBoxLayout())
     
        HMAC_KEY = persistReg.getObject("PyroNSWidget.HMAC_KEY")
        if HMAC_KEY is None:
            HMAC_KEY = ""
        
        self.hmacKeyWidget  = QtGui.QLineEdit(HMAC_KEY)    
        self.startBtn       = QtGui.QPushButton("Start")
        self.stopBtn        = QtGui.QPushButton("Stop")      

        self.layout().addWidget(self.hmacKeyWidget)
        self.layout().addWidget(self.startBtn)
        self.layout().addWidget(self.stopBtn)

        self.stopBtn.setEnabled(False)

        self.startBtn.clicked.connect(self.startNSServer)
        self.stopBtn.clicked.connect(self.stopNSServer) 
        
        self.hmacKeyWidget.textChanged.connect(self.hmacKeyChanged)
        
        self.nameServer = None

        sys.excepthook                      = Pyro4.util.excepthook    
        Pyro4.config.HMAC_KEY               = str(self.hmacKeyWidget.text())
        Pyro4.config.THREADPOOL_MAXTHREADS  = 500



    def registerObject(self, objectToRegister, objectName):
        self.checkNameServer()
        self.nameServer.registerObject(objectToRegister, objectName)



    def hmacKeyChanged(self):
        persistReg.setObject("PyroNSWidget.HMAC_KEY", self.hmacKeyWidget.text())
        Pyro4.config.HMAC_KEY = str(self.hmacKeyWidget.text())



    def startNSServer(self):

        NSisDeamon=False
        
        """
         When not specifying the ports for pyro servers, ports are attributed
         randomly. However, they can fall on already attributed port, in which
         case i raise an exception. Try ten time to get a valid port, and else
         inform the user of the situation.
        """
        for i in range(10):
            try:
                self.nameServer = PyroServer(socket.gethostbyname(socket.gethostname()), NSisDeamon)
            except socket_error  as e:
                if i < 9 :
                    continue
                else:
                    QtGui.QMessageBox.critical(self, "Error starting the name server socket.", str(e))   
                    raise
                    return
            break        
        
            
            
        self.nameServer.start()
        
        time.sleep(2.5)

        for i in range(100):
            try:
                Pyro4.locateNS()
                self.hmacKeyWidget.setEnabled(False)    

                self.nameServer.registerObject(StagingRoom(), "stagingRoom")
                self.nameServer.registerObject(CommunicationHub(), "communicationHub")
                
                self.mainWindow.consumerWidget.connectHub(str(self.hmacKeyWidget.text()), 
                                                          "communicationHub")
                
                self.mainWindow.schemaView.comHubName       = "communicationHub"
                self.mainWindow.schemaView.stagingRoomName  = "stagingRoom"
                self.mainWindow.schemaView.HMAC_KEY         = str(self.hmacKeyWidget.text())                
                    
                self.startBtn.setEnabled(False)
                self.stopBtn.setEnabled(True)
                
                return 
            except Pyro4.errors.PyroError:
                time.sleep(0.1)
                
        QtGui.QMessageBox.critical(self, "Error launching Pyro's name server.", 
                                     "After waiting 10 seconds, the server was still not running.")            



    def nsIsRunning(self): 
        try:
            Pyro4.locateNS()
            return True
        except Pyro4.errors.PyroError:
            return False
            
            

    def stopNSServer(self):
        self.nameServer.stopNS()        
        self.hmacKeyWidget.setEnabled(True)    
        self.startBtn.setEnabled(True)
        self.stopBtn.setEnabled(False)
        self.nameServer = None
        self.mainWindow.schemaView.comHubName       = None
        self.mainWindow.schemaView.stagingRoomName  = None
        self.mainWindow.schemaView.HMAC_KEY         = None


    def checkNameServer(self):

        if not self.nsIsRunning():
            msgBox = QtGui.QMessageBox()
            msgBox.setText("Pyro's nameserver is not running.")
            msgBox.setInformativeText("Do you want to start it?")
            msgBox.setStandardButtons(QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
            msgBox.setDefaultButton(QtGui.QMessageBox.Yes)
            ret = msgBox.exec_()            
            
            if ret == QtGui.QMessageBox.Yes:
                self.startNSServer()
                if not self.nsIsRunning():
                    return False
            elif ret == QtGui.QMessageBox.No:
                msgBox = QtGui.QMessageBox()
                msgBox.setText("The schema could not be started.")
                msgBox.setInformativeText("A Pyro nameserver must be running for schema to be process.")
                msgBox.setStandardButtons(QtGui.QMessageBox.Ok)
                msgBox.setDefaultButton(QtGui.QMessageBox.Ok)
                ret = msgBox.exec_()            
                return False


        
        if self.nameServer is None:
            """
            A server is running but it has been started outside this program. 
            It is fine, as long as it has been started with the same HMAC_KEY.
            """
            #TODO: Verify that the pyro objects "communicationHub" and 
            # "stagingRoom" exists. <== Must raise an exception until this TODO has been done.            
            msgBox = QtGui.QMessageBox()
            msgBox.setText("Problem starting the name server.")
            msgBox.setInformativeText("A name server is running but it has been started from outside this program.")
            msgBox.setStandardButtons(QtGui.QMessageBox.Ok)
            msgBox.setDefaultButton(QtGui.QMessageBox.Ok)
            ret = msgBox.exec_()            
            return False

        return True




class PyperWidget(QtGui.QWidget):
    
    def __init__(self, parent = None):
        super(PyperWidget, self).__init__(parent)        
        
        
        self.setLayout(QtGui.QHBoxLayout())
      
        self.layout().addWidget(QtGui.QLabel("Pypers:"))







class MainWindow(QtGui.QMainWindow):

    def __init__(self, *args):
        apply(QtGui.QMainWindow.__init__, (self, ) + args)
        self.setWindowTitle('EEG Analyzer') 


        self.host = socket.gethostbyname(socket.gethostname())

        self.mainWidget = QtGui.QWidget(self)

        self.setCentralWidget (self.mainWidget)
        
        
 
        self.undoAct = QtGui.QAction("&Undo", self,
                shortcut=QtGui.QKeySequence.Undo,
                statusTip="Undo the last operation", triggered=self.undo)
 
        self.redoAct = QtGui.QAction("&Redo", self,
                shortcut=QtGui.QKeySequence.Redo,
                statusTip="Redo the last operation", triggered=self.redo)
 
        self.cutAct = QtGui.QAction("Cu&t", self,
                shortcut=QtGui.QKeySequence.Cut,
                statusTip="Cut the current selection's contents to the clipboard",
                triggered=self.cut)
 
        self.copyAct = QtGui.QAction("&Copy", self,
                shortcut=QtGui.QKeySequence.Copy,
                statusTip="Copy the current selection's contents to the clipboard",
                triggered=self.copy)
 
        self.pasteAct = QtGui.QAction("&Paste", self,
                shortcut=QtGui.QKeySequence.Paste,
                statusTip="Paste the clipboard's contents into the current selection",
                triggered=self.paste)        
        
        
 
        self.mainLayout     = QtGui.QVBoxLayout()
        
        self.databaseWidget = DatabaseWidget(self) 
        self.schemaWidget   = SchemaWidget(self)
        self.pyroNSWidget   = PyroNSWidget(self)
        self.pyperWidget    = PyperWidget(self)
        self.nodesWidget    = ProcessingNodeWidget(self)
        self.consumerWidget = ComConsumerWidget(self)
    
        self.docks =  {"Pyro":QtGui.QDockWidget("Pyro properties", self),
                       "Pypers":QtGui.QDockWidget("Pypers", self),
                       "Nodes":QtGui.QDockWidget("Processing nodes", self) }
                       
                       
        self.schemaView = SchemaView(self, None, None)  
        self.schemaView.startRegisterRequest.connect(self.startRegister)                       
                       
        self.mainTabs = QtGui.QTabWidget()
        self.mainTabs.addTab(self.consumerWidget, "Processing node output")
        self.mainTabs.addTab(self.schemaView, "Schema")
        
        self.docks["Pyro"].setWidget(self.pyroNSWidget)        
        self.docks["Pypers"].setWidget(self.pyperWidget)       
        self.docks["Nodes"].setWidget(self.nodesWidget)        
        
        self.addDockWidget(QtCore.Qt.BottomDockWidgetArea, self.docks["Pyro"])
        self.addDockWidget(QtCore.Qt.BottomDockWidgetArea, self.docks["Pypers"])
        self.addDockWidget(QtCore.Qt.BottomDockWidgetArea, self.docks["Nodes"])
        
        self.docks["Pyro"].hide()
        self.docks["Pypers"].hide()
        self.databaseWidget.hide()
        
        self.mainLayout.addWidget(self.databaseWidget)
        self.mainLayout.addWidget(self.schemaWidget)
        self.mainLayout.addWidget(self.mainTabs)

        self.mainWidget.setLayout(self.mainLayout)

        self.showMaximized()



    def startRegister(self):
        schemaName   = self.schemaWidget.schemaListWidget.currentItem().text()        
        registerName = self.schemaWidget.schemaDict[schemaName].registerName
        register     = Register(self.schemaWidget.schemaDict[schemaName].registerPath)
        self.pyroNSWidget.registerObject(register, registerName)





    def closeEvent(self, event):
        reply = QtGui.QMessageBox.question(self, 'Message',
            "Are you sure to want to quit?", QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)

        if reply == QtGui.QMessageBox.Yes:
            self.nodesWidget.stopLocalNode()
            if not self.pyroNSWidget.nameServer is None:
                self.pyroNSWidget.nameServer.stopNS()
            event.accept()
            #QtCore.QCoreApplication.exit() 
        else:
            event.ignore()
            


    def undo(self):
        QtGui.QApplication.focusWidget().undo()
 
    def redo(self):
        QtGui.QApplication.focusWidget().redo()
 
    def cut(self):
        QtGui.QApplication.focusWidget().cut()
 
    def copy(self):
        QtGui.QApplication.focusWidget().copy()
 
    def paste(self):
        QtGui.QApplication.focusWidget().paste()



    def checkNameServer(self):
        return self.pyroNSWidget.checkNameServer()

            




def main(args):    
    app = QtGui.QApplication(args)
    win = MainWindow()
    win.show()  
    sys.exit(app.exec_())
    



def startAnalyzer():
    main([])



if __name__ == '__main__':
    arg = sys.argv
    main(arg)

#import cProfile
#if __name__ == '__main__':   
#    cProfile.run('main(sys.argv)', 'profileRepport')


