# -*- coding: utf-8 -*-
"""
Created on Wed Oct 16 22:47:50 2013

@author: Christian
"""

import socket               # Import socket module
import Pyro4
import sys
from PyBlockWork import ProcessingNode


class ProcessingServer:

    def __init__(self, port=9345):
        self.socket = socket.socket() 
        self.host   = socket.gethostbyname(socket.gethostname())
        self.port   = port
        self.socket.bind((self.host, self.port))
        self.commands = []
        
        self.stagingRoomName = 'stagingRoom'
        self.comHubName = 'communicationHub'
        self.HMAC_KEY = ''
        self.nbProcessingNodes = 1
        
        sys.excepthook                      = Pyro4.util.excepthook   
        Pyro4.config.THREADPOOL_MAXTHREADS  = 500 
        
        
    def listen(self):
        print "Listening..."
        self.socket.listen(5)                 # Now wait for client connection.
        
        self.connection, self.clientAddress = self.socket.accept()     # Establish connection with client.
        print "Connected..."
        while True:
            message = self.connection.recv(4096)
            
            if message != "":
                self.commands.extend(message.splitlines())
                
            if len(self.commands):
                command = self.commands[0]
                self.commands = self.commands[1:]
                
                tokens = command.split()                

                print "Command: ", command                
                
                if tokens[0] == "checkNameServer":
                    self.checkNameServer()
                    
                elif tokens[0] == "set":
                    if len(tokens) == 2 :
                        tokens.append("")
                        
                    if tokens[1] == "nbProcessingNodes":
                        self.nbProcessingNodes = int(tokens[2])
                    else:
                        setattr(self, tokens[1], tokens[2])   
                        
                    if tokens[1] == "HMAC_KEY":                        
                        Pyro4.config.HMAC_KEY = tokens[2]
                                             
                    self.sendCommand("OK")
                    
                elif tokens[0] == "startProcessingNodes":
                    self.startProcessingNodes()
                    
                elif tokens[0] == "stopProcessingNodes":
                    self.stopProcessingNodes()
                    
                
    def getAddress(self):
        return self.host  + ":" + str(self.port)
        
    """
    @property
    def nbProcessingNodes(self):
        return self.__nbProcessingNodes
        
    @nbProcessingNodes.setter
    def nbProcessingNodes(self, value):
        self.__nbProcessingNodes = int(value)  
    

    @property
    def HMAC_KEY(self):
        return self.__HMAC_KEY
        
    @HMAC_KEY.setter
    def HMAC_KEY(self, value):
        self.__HMAC_KEY = value  
        Pyro4.config.HMAC_KEY = value
    """
    

    def checkNameServer(self):
        try:
            Pyro4.locateNS()
            self.sendCommand("OK")
        except Pyro4.errors.PyroError:
            self.sendCommand("Failed")     


    def startProcessingNodes(self):

        pyroInfos={"stagingRoomName":self.stagingRoomName, 
                   "comHubName":self.comHubName,
                   "HMAC_KEY":self.HMAC_KEY}
                                             
        self.node = ProcessingNode(self.nbProcessingNodes, pyroInfos)

        self.sendCommand("OK")


    def stopProcessingNodes(self):
          
        self.node.terminate()
        self.sendCommand("OK")





    def read(self):
        return self.socket.recv(1024)
        
    def sendCommand(self, message):
        print "sending:", message
        self.connection.send(message + "\n")
        
    def close(self):
        print "Closing..."
        self.socket.close()
 