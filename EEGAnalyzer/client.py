# -*- coding: utf-8 -*-
"""
Created on Wed Oct 16 22:49:08 2013

@author: Christian
"""


import socket               # Import socket module

class ProcessingClient:

    def __init__(self, host, port=9345):
        self.socket  = socket.socket() 
        self.host    = host
        self.port    = port
        self.buffer  = ""
        self.commands = []
        
    def connect(self):
        self.socket.connect((self.host, self.port))
        
    def read(self):
        return self.socket.recv(1024)
        
    def sendCommand(self, command):
        self.socket.sendall(command + "\n")       
        if not self.waitFeedback() :
            raise ValueError("Command '" + command + "' could not be executed by the server.")
            
    
    def waitFeedback(self):
        while len(self.commands) == 0:
            self.buffer += self.socket.recv(4096)
            tokens = self.buffer.split("\n")
            
            if len(tokens) == 1:
                self.buffer = tokens[0]
            else:
                self.buffer = tokens[-1]
                self.commands.extend(tokens[:-1])
           
        command         = self.commands[0]
        self.commands   = self.commands[1:]
        if command == "OK":
            return True
        else:
            return False
            
            
    def close(self):
        self.socket.close()
