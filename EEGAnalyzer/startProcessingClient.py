# -*- coding: utf-8 -*-
"""
Created on Fri Oct 18 17:49:12 2013

@author: oreichri
"""



from client import ProcessingClient



client = ProcessingClient("localhost")

client.connect()

client.sendCommand("checkNameServer")
client.sendCommand("set stagingRoomName stagingRoom")
client.sendCommand("set comHubName communicationHub")
client.sendCommand("set HMAC_KEY key")
client.sendCommand("set nbProcessingNodes 2")
client.sendCommand("startProcessingNodes")