from setuptools import setup
import os

PACKAGE = "EEGAnalyzer"
NAME = "EEGAnalyzer"
DESCRIPTION = "Graphical interface merging the functions of Spyndle, PyBlockWork and EEG Navigator in one convenient interface, allowing to perform efficiently analysis of electroencephalographic data."
AUTHOR = "Christian O'Reilly"
AUTHOR_EMAIL = "christian.oreilly@umontreal.ca"
VERSION = __import__(PACKAGE).__version__
URL = "https://bitbucket.org/christian_oreilly/eeg-analyzer/"
DOWNLOAD_URL = URL + "downloads/" + NAME + "-" + VERSION + ".zip"

def is_package(path):
    return (
        os.path.isdir(path) and
        os.path.isfile(os.path.join(path, '__init__.py'))
        )

def find_packages(path, base="" ):
    """ Find all packages in path """
    packages = {}
    for item in os.listdir(path):
        dir = os.path.join(path, item)
        if is_package( dir ):
            if base:
                module_name = "%(base)s.%(item)s" % vars()
            else:
                module_name = item
            packages[module_name] = dir
            packages.update(find_packages(dir, module_name))
    return packages

packages=find_packages(".")


setup(
    name=NAME,
    packages=packages.keys(),
    package_dir=packages,
    version=VERSION,
    description=DESCRIPTION,
    long_description=open("README.txt").read(),
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    license='LICENSE.txt',
    classifiers=["Development Status :: 2 - Pre-Alpha",
                "Environment :: MacOS X",
                "Environment :: Win32 (MS Windows)",
                "Environment :: X11 Applications",
                "Intended Audience :: Science/Research",
                "License :: Free for non-commercial use",
                "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
                "Natural Language :: English",
                "Programming Language :: Python :: 2.7",
                "Topic :: Scientific/Engineering",
                "Topic :: Scientific/Engineering :: Visualization"],
	download_url=DOWNLOAD_URL,		
    url=URL)
